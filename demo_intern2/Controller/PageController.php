<?php
include_once dirname(__DIR__)."\Models\page.php" ;

class PageController{

	private $page;
	function __construct(){
		$this->page = new page();
	}
	public function countall($search,$status,$start,$limit){
		 
			$kq = $this->page->CountAll($search,$status,$start,$limit);
			return $kq;
	}
	public function createArea($name, $width,$height,$style,$number_sub){
		 
			$kq = $this->page->insertArea($name, $width,$height,$style,$number_sub);
			return $kq;
	}
	public function createBanner($name, $image,$linkImage,$id_area,$width,$height){
		 
			$kq = $this->page->insertBanner($name, $image,$linkImage,$id_area,$width,$height);
			return $kq;
	}
	public function createUser($username,$password,$level,$code){
		 
			$kq = $this->page->insertUser($username,$password,$level,$code);
			return $kq;
	}
	public function createFollowApprove($ar1,$ar2,$ar3,$name,$uploader,$master){
		 
			$kq = $this->page->insertFollowApprove($ar1,$ar2,$ar3,$name,$uploader,$master);
			return $kq;
	}
	
	public function searchAndstatus($search,$status,$start,$limit){
		 
			$kq = $this->page->SearchAndStatus($search,$status,$start,$limit);
			return $kq;
	}
	public function countsearchAndstatus($search,$status){
		 
		$kq = $this->page->countsearchAndstatus($search,$status);
		return $kq;
	}
	public function create($title, $content,$status,$newPath,$template,$user_id){
			$kq = $this->page->insertPage($title, $content,$status,$newPath,$template,$user_id);
			return $kq;
	}
	public function createOrganization($name,$code,$id_temp,$folder){
			$kq = $this->page->insertOrganization($name,$code,$id_temp,$folder);
			return $kq;
	}
	public function createVer($title, $content,$ver,$page_id){
			$kq = $this->page->insertVersion($title, $content,$ver,$page_id);
			return $kq;
	}
	public function createTemp($name, $path_temp,$kind,$follow_id){
		 
			$kq = $this->page->insertTemp($name, $path_temp,$kind,$follow_id);
			return $kq;
	}
	public function createAutoLink($page_id, $page_link,$position){
		 
			$kq = $this->page->insertAutoLink($page_id, $page_link,$position);
			return $kq;
	}
	public function createAutoLinkDepartment($page_link,$name,$status,$kind,$code){
		 
			$kq = $this->page->insertAutoLinkDepartment($page_link,$name,$status,$kind,$code);
			return $kq;
	}
	public function edit($id){
			
			$kq = $this->page->editPage($id);
			return $kq;
	}

	// public function updateAutoLink($position,$page_link){
	// 		$kq = $this->page->EditAutoLink($position,$page_link);
	// 		return $kq;
	// }
	public function updateAutoLinkDepartment($id,$page_link,$name,$status,$kind,$dept_code){
			$kq = $this->page->updateAutoLink($id,$page_link,$name,$status,$kind,$dept_code);
			return $kq;
	}
	public function updateFollowApproveDepartment($id,$ar1,$ar2,$ar3,$name,$uploader,$master){
		$kq = $this->page->updateFollowApprove($id,$ar1,$ar2,$ar3,$name,$uploader,$master);
		return $kq;	
	}
	public function updateUserID($id,$username,$password,$level,$code){
			$kq = $this->page->updateUser($id,$username,$password,$level,$code);
			return $kq;
	}
	public function updateUnPublish($id,$link,$status){
			$kq = $this->page->UnPublish($id,$link,$status);
			return $kq;
	}
	public function update($id,$content,$title,$status){
			$kq = $this->page->updatePage($id,$content,$title,$status);
			
			return $kq;
	}
	public function updateDepart($id,$name,$template){
			$kq = $this->page->updateDepartment($id,$name,$template);
			return $kq;
	}
	public function updateVer($id,$content,$title){
			$kq = $this->page->updateVersion($id,$content,$title);
			
			return $kq;
	}
	public function delete($id){
			$kq = $this->page->deletePage($id);
			return $kq;
	}

	public function link($id,$link,$status){
			$kq = $this->page->updateLink($id,$link,$status);
			return $kq;
	}
	public function pagination($start,$limit){
			$kq = $this->page->paging($start,$limit);
			return $kq;
	}

	public function search($search,$start,$limit){
			$kq = $this->page->searchPage($search,$start,$limit);
			return $kq;
	}
	public function searchstatus($search,$start,$limit){
			$kq = $this->page->searchStatus($search,$start,$limit);
			return $kq;
	}
	public function count($search){
			$kq = $this->page->countSearch($search);
			return $kq;
	}
	public function countStatus($search){
			$kq = $this->page->countSearchStatus($search);
			return $kq;
	}
}
?>