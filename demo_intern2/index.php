<?php
 ob_start();
$request = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);

switch ($request) {
    case '/':
        require __DIR__ . '/Views/Fanpage/list.php';
        break;
    case '/login':
        require __DIR__ . '/Views/User/login.php';
        break;  
    case '/logout':
        require __DIR__ . '/Views/User/logout.php';
        break;     
    case '/check-user':
        require __DIR__ . '/Views/User/checkUser.php';
        break;  
    case '/get-template':
        require __DIR__ . '/Views/Fanpage/getTemplate.php';
        break;
    case '/create-folder':
        require __DIR__ . '/Views/Fanpage/createFolder.php';
        break;
    case '/create-file':
        require __DIR__ . '/Views/Fanpage/createFile.php';
        break;    
    case '/open-folder':
        require __DIR__ . '/Views/Fanpage/openfolder.php';
        break; 
    case '/create-template' :
        require __DIR__ . '/Views/Fanpage/createTemplate.php';
        break;
    case '/list-template' :
        require __DIR__ . '/Views/Fanpage/listTemplate.php';
        break;
    case '/get-content' :
        require __DIR__ . '/Views/Fanpage/getContent.php';
        break;
    case '/get-content-version' :
        require __DIR__ . '/Views/Fanpage/getContentVersion.php';
        break;
    case '/validate-title' :
        require __DIR__ . '/Views/Fanpage/validateTitle.php';
        break;
    case '/create-page' :
        require __DIR__ . '/Views/Fanpage/createPage.php';
        break;
    case '/edit' :
        require __DIR__ . '/Views/Fanpage/edit.php';
        break;
    case '/Auto-Link' :
        require __DIR__ . '/Views/Fanpage/autoLink.php';
        break;
     case '/checkAutoLink' :
        require __DIR__ . '/Views/Fanpage/checkAutoLink.php';
        break;
    case '/checkPosition' :
        require __DIR__ . '/Views/Fanpage/checkPosition.php';
        break;
    case '/checkListPosition' :
        require __DIR__ . '/Views/Fanpage/checkListPosition.php';
        break;
    case '/create-AutoLink' :
        require __DIR__ . '/Views/Fanpage/create-AutoLink.php';
        break;
    case '/list-AutoLink' :
        require __DIR__ . '/Views/Fanpage/list-AutoLink.php';
        break;
     case '/version' :
        require __DIR__ . '/Views/Fanpage/versionPage.php';
        break;
    case '/validate-edit-title' :
        require __DIR__ . '/Views/Fanpage/validateEditTitle.php';
        break;
    case '/upload' :
        require __DIR__ . '/Views/Fanpage/upload.php';
        break;
    case '/create-user' :
        require __DIR__ . '/Views/Fanpage/create_user.php';
        break;
    case '/check-create-user' :
        require __DIR__ . '/Views/Fanpage/checkCreateUser.php';
        break;
    case '/list-user' :
        require __DIR__ . '/Views/Fanpage/list_user.php';
        break;
    case '/edit-user' :
        require __DIR__ . '/Views/Fanpage/edit_user.php';
        break;
    case '/check-edit-user' :
        require __DIR__ . '/Views/Fanpage/checkEditUser.php';
        break;
    case '/organization' :
        require __DIR__ . '/Views/Fanpage/organization.php';
        break;
    case '/get-value-tc2' :
        require __DIR__ . '/Views/Fanpage/getValueTC2.php';
        break;
    case '/get-value-tc3' :
        require __DIR__ . '/Views/Fanpage/getValueTC3.php';
        break;
    case '/check-exist-code' :
        require __DIR__ . '/Views/Fanpage/checkExistCode.php';
        break;
    case '/list-department' :
        require __DIR__ . '/Views/Fanpage/listDepartment.php';
        break;
    case '/Auto-Link-Department' :
        require __DIR__ . '/Views/Fanpage/autoLinkDepartment.php';
        break;
    case '/get-Page-AutoLink' :
        require __DIR__ . '/Views/Fanpage/getPageAutoLink.php';
        break;
    case '/get-Page-Department' :
        require __DIR__ . '/Views/Fanpage/getPageDepartment.php';
        break;
    case '/create-autoLink-department' :
        require __DIR__ . '/Views/Fanpage/createAutoLinkDepartment.php';
        break;
    case '/list-autoLink-department' :
        require __DIR__ . '/Views/Fanpage/listAutoLinkDepartment.php';
        break;
    case '/edit-autolink-department' :
        require __DIR__ . '/Views/Fanpage/editAutoLinkDepartment.php';
        break;
    case '/get-page-edit-autolink-department' :
        require __DIR__ . '/Views/Fanpage/getPageEditAutoLink.php';
        break;
    case '/update-page-autolink-department' :
        require __DIR__ . '/Views/Fanpage/updateAutoLinkDepartment.php';
        break;
    case '/get-value-edit-tc2' :
        require __DIR__ . '/Views/Fanpage/getValueEditTC2.php';
        break;
    case '/get-value-edit-tc3' :
        require __DIR__ . '/Views/Fanpage/getValueEditTC3.php';
        break;
    case '/get-page-edit-department' :
        require __DIR__ . '/Views/Fanpage/getPageEditDepartment.php';
        break;
    case '/approve' :
        require __DIR__ . '/Views/Fanpage/approvePage.php';
        break;
    case '/edit-department' :
        require __DIR__ . '/Views/Fanpage/editDepartment.php';
        break;
    case '/get-value-edit-department-tc2' :
        require __DIR__ . '/Views/Fanpage/getValueEditDepartmentTC2.php';
        break;
    case '/get-value-edit-department-tc3' :
        require __DIR__ . '/Views/Fanpage/getValueEditDepartmentTC3.php';
        break;
    case '/get-template-edit-department' :
        require __DIR__ . '/Views/Fanpage/getTemplateEditDepartment.php';
        break;
    case '/check-exist-folder' :
        require __DIR__ . '/Views/Fanpage/checkExistFolder.php';
        break;
    case '/csv' :
        require __DIR__ . '/Views/Fanpage/csv.php';
        break;
    case '/export' :
        require __DIR__ . '/Views/Fanpage/export.php';
        break;
    case '/import' :
        require __DIR__ . '/Views/Fanpage/import.php';
        break;
    case '/create-follow-approve' :
        require __DIR__ . '/Views/Fanpage/followApprove.php';
        break;
    case '/list-follow-approve' :
        require __DIR__ . '/Views/Fanpage/listFollowApprove.php';
        break;
    case '/edit-follow-approve' :
        require __DIR__ . '/Views/Fanpage/editFollowApprove.php';
        break;
    case '/check-uploader' :
        require __DIR__ . '/Views/Fanpage/checkUploader.php';
        break;
    case '/check-user-uploader' :
        require __DIR__ . '/Views/Fanpage/checkUserUploader.php';
        break;
    case '/ads' :
        require __DIR__ . '/Views/Fanpage/ads.php';
        break;
    case '/create-area' :
        require __DIR__ . '/Views/Fanpage/createArea.php';
        break;
    case '/create-banner' :
        require __DIR__ . '/Views/Fanpage/createBanner.php';
        break;
    case '/list-banner' :
        require __DIR__ . '/Views/Fanpage/listBanner.php';
        break;
    case '/edit-area' :
        require __DIR__ . '/Views/Fanpage/editArea.php';
        break;
    case '/edit-banner' :
        require __DIR__ . '/Views/Fanpage/editBanner.php';
        break;
    case '/get-image' :
        require __DIR__ . '/Views/Fanpage/getImage.php';
        break;
    case '/auto-cron-feedback' :
        require __DIR__ . '/Views/cron/auto_cron_feedback.php';
        break;
    case '/feedback' :
        require __DIR__ . '/Views/Fanpage/feedback.php';
        break;
    default:
        echo "fail";
        break;
}

?>