<html>
	<head>
		<meta charset="utf-8">
		<title>Demo Internship</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
		
    	<!-- <script type="text/javascript" src="https://connect.facebook.net/en_US/sdk.js"></script> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
		<!-- Thư viện jquery đã nén phục vụ cho bootstrap.min.js  -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	    <!-- Thư viện popper đã nén phục vụ cho bootstrap.min.js -->
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
	    <!-- Bản js đã nén của bootstrap 4, đặt dưới cùng trước thẻ đóng body-->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
	    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
		<script src="http://demointern.com/ckeditor/ckeditor.js"></script>
		

</head>
	
<body >
	<style type="text/css">
		/*div.content{
			width: 100%;
			background: black;
			color:orange;
			text-align: center;	
			font-size: 40px;
			font-weight: bold;	
		}*/
		/*body{
			style="background: radial-gradient(#97f40387, #03a9f4b0);"
			background-image: url('https://br-art.vn/wp-content/uploads/2017/06/background-hinh-nen-wallpaper-thien-nhien-phong-canh-dep-1-1024x561.jpg');
			background-repeat: no-repeat; 
			background-size: 100%;
		}*/
		/*.btn-group{

		}*/
	</style>
	<?php
	include_once dirname(__DIR__)."\Models\page.php";
	$page = new page();
	if(isset($_SESSION['email'])){
 		$getUserID = $page->selectUserID($_SESSION['email']);
 	}
    ?>
<?php if(isset($_SESSION['email'])){ ?>	

 <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background: #eeeeee;">

  <div class="container-fluid" style="display: inline-block;">
  	
   	<div class="btn-group" style="border: 4px solid #99ccff; border-radius: 10px;">
   		<a class="btn btn-default" style="font-size: 38px;" href="/"><i class="fas fa-home"></i></a>
  	</div>
	
	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/list-user">User</a>
       	<?php } ?>
  	</div>
  	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/list-template">Template</i></a> 
       	<?php } ?>
  	</div>
  	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/list-autoLink-department" >Auto Link</a>
       	<?php } ?>
  	</div>
 	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/list-department">Department</a>
       	<?php } ?>
  	</div>
  	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/csv">CSV File</i></a>
       	<?php } ?>
  	</div>
 
  	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/list-banner">Banner</a>
       	<?php } ?>
  	</div>
  	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/ads">Area</a>
       	<?php } ?>
  	</div>
  	<div class="btn-group">
    	<?php if( $getUserID[0]['level'] == 1 ){ ?>
        	<a class="btn btn-light" href="/list-follow-approve">Follow</a>
       	<?php } ?>
  	</div>
    <div class="btn-group">
      <?php if( $getUserID[0]['level'] == 1 ){ ?>
          <a class="btn btn-light" href="/feedback">Feedback</a>
        <?php } ?>
    </div>
  	<div class="btn-group" style="float:right; margin-top: -10px;">
   		 <?php
   		 	if(!empty($_SESSION['email'])){
   		 ?>
	   		 <span style="font-weight: bold;" >Hello: <?php echo $_SESSION['email']; ?>
	    		<a  href="/Views/User/logout.php"><i class="fas fa-sign-in-alt"></i></a>
	    	</span>
    	<?php
     		}
    	?>
  	</div>
  </div>

</nav>
<?php } ?>
   