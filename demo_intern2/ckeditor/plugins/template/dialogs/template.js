CKEDITOR.dialog.add( 'Abcdialog', function( editor ) {
    return {
        title: 'Code Template',
        minWidth: 600,
        minHeight: 400,

        contents: [
            {
                elements: [
                    {
                        type: 'radio',
                        id:   'template',
                        items: [
                         <?php foreach($template as $value){
                            echo "['$value[name]'],";  

                        }
                        ?>
                          
                                ],
                        onClick: function(a) {
                            var name = $( "input[type=radio][name=template_radio]:checked" ).val();

                            var content = $('#show_content').contents().find('body');
                            $.ajax({
                              type:'post',
                              url:'/ckeditor/plugins/template/dialogs/get_test.php',
                              data:{name:name},
                              success:function(response){
                               
                                  content.html(response);
                              }
                            });
                        },
                        
                    },
                    {
                        type: 'html',
                        html: "<iframe id='show_content' style='width:100%; -webkit-transform: scale(0.75); height:300px; border:1px solid black;'></iframe>"
                    },
                    
                ],
            },
        ],
        onOk:function(){
            var content = $('#show_content').contents().find('body').html();
            editor.insertHtml(content);       
        }
    };
});
