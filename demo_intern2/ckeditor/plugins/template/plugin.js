CKEDITOR.plugins.add( 'template', {
    icons: 'template',
    init: function( editor ) {
        editor.addCommand( 'template', new CKEDITOR.dialogCommand( 'Abcdialog' ) );
        editor.ui.addButton( 'Template', {
        label: 'Template',
        command: 'template',
        toolbar: 'insert'
    });
    CKEDITOR.dialog.add( 'Abcdialog', this.path + 'dialogs/Get_template.php' );
    }
});