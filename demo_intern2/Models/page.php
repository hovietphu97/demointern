<?php
include_once dirname(__DIR__)."\config\database.php"  ;
class page extends database{
	

	private $connect;	


	function __construct(){
		$this->connect = new database();
		if($this->connect->connection()){
			return true;
		}else{
			return  false;
		}
	}
	public function insertArea($name, $width,$height,$style,$number_sub){

		$sql = "INSERT INTO area(name,width,height,style,number_sub) VALUES('".$name."','". $width."','".$height."','".$style."','".$number_sub."')";

		$result = $this->connect->connection()->query($sql);
		
		return $result;
	}
	public function insertAutoCron($page_id, $question,$answer,$number_of){

		$sql = "INSERT INTO auto_cron(page_id,question,answer,number_of) VALUES('".$page_id."','". $question."','".$answer."','".$number_of."')";

		$result = $this->connect->connection()->query($sql);
		
		return $result;
	}
	public function insertBanner($name, $image,$linkImage,$id_area,$width,$height){

		$sql = 'INSERT INTO banner(name,image,link_image,id_area,width,height) VALUES("'.$name.'","'. $image.'","'.$linkImage.'","'.$id_area.'","'.$width.'","'.$height.'")';

		$result = $this->connect->connection()->query($sql);
		return $result;
	}
	public function insertPage($title, $content,$status,$newPath,$template,$user_id){
        
		$title = mysqli_real_escape_string( $this->connect->connection(), $title);
	
		$sql = "INSERT INTO page(title,content,status,new_path,template,user_id) VALUES('".$title."','". $content."','".$status."','".$newPath."','".$template."','".$user_id."')";

		$result = $this->connect->connection()->query($sql);
		
		return $result;
	}

	public function insertUser($username, $password,$level,$code){

		$sql = "INSERT INTO user(username,password,level,code) VALUES('".$username."','". $password."','".$level."','".$code."')";
		$result = $this->connect->connection()->query($sql);
		return $result;
	}
	public function insertVersion($title, $content,$ver,$idPage){
        
		$title = mysqli_real_escape_string( $this->connect->connection(), $title);
	
		$sql = "INSERT INTO version(title,content,ver,page_id) VALUES('".$title."','". $content."','".$ver."','".$idPage."')";

		$result = $this->connect->connection()->query($sql);
		
		return $result;
	}
	public function insertTemp($name, $path_temp,$kind,$follow_id){
        
		$name = mysqli_real_escape_string( $this->connect->connection(), $name);
	
		$sql = "INSERT INTO template(temp_name,temp_path,kind,follow_id) VALUES('".$name."','". $path_temp."','". $kind."','". $follow_id."')";

		$result = $this->connect->connection()->query($sql);
		
		return $result;
	}
	public function insertAutoLink($page_id, $page_link,$position){
        
	
		$sql = "INSERT INTO auto_link(page_id,page_link,position) VALUES('".$page_id."','". $page_link."','". $position."')";

		$result = $this->connect->connection()->query($sql);
		return $result;
	}
	public function insertAutoLinkDepartment($page_link,$name,$status,$kind,$code){
        
	
		$sql = "INSERT INTO auto_link(page_link,name,status,kind,dept_code) VALUES('". $page_link."','". $name."','". $status."','". $kind."','". $code."')";

		$result = $this->connect->connection()->query($sql);
		return $result;
	}
	public function insertOrganization($name, $code, $id_temp,$folder){
		$sql = "INSERT INTO organization(name_tc,code_tc,template_id,folder) VALUES('".$name."','". $code."','". $id_temp."','".$folder."')";

		$result = $this->connect->connection()->query($sql);
		return $result;
	}
	public function insertFollowApprove($ar1, $ar2, $ar3,$name,$uploader,$master){
		$sql = "INSERT INTO follow(ar1,ar2,ar3,name,id_uploader,master) VALUES('".$ar1."','". $ar2."','". $ar3."','".$name."','".$uploader."','".$master."')";

		$result = $this->connect->connection()->query($sql);
		return $result;
	}

	public function EditAutoLink($position,$page_link){
		$sql = "UPDATE `auto_link`
                    SET 
                    `position`='".$position."'
                    WHERE `page_link` = '".$page_link."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function selectListArea(){
		$sql = "SELECT * FROM area";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	// public function selectCronFeeback(){
	// 	$sql = "SELECT * FROM auto_cron";
	// 	$query = mysqli_query($this->connect->connection(),$sql);
	// 	$array = array();
	// 	while($row = mysqli_fetch_assoc($query)){
	// 		$array[] = $row;
	// 	}
	// 	return $array;
	// }
	// SELECT page_id, question, CONCAT(answer , ':' , sum(number_of)) AS demo FROM `auto_cron` GROUP BY page_id,question,answer
	public function selectCronFeeback(){
		// $sql = "SELECT page_id, question,answer, sum(number_of) FROM auto_cron GROUP BY page_id,question,answer";
		$sql = "SELECT page_id, question, convert( group_concat( distinct demo order by demo ) Using utf8 )as demo From ( SELECT DISTINCT page_id, question, CONCAT(answer , ':' , sum(number_of)) AS demo FROM auto_cron GROUP BY page_id,question,answer ) as a GROUP BY page_id, question";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectAreaByID($id){
		$sql = "SELECT * FROM area WHERE id =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectListBanner(){
		$sql = "SELECT * FROM banner";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectListBannerByAreaID($id){
		$sql = "SELECT * FROM banner WHERE id_area = ".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectAutoLink($id){
		$sql = "SELECT * FROM auto_link WHERE id =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}

	public function selectAutoLinkDepartment(){
		$sql = "SELECT * FROM auto_link";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectOrganization(){
		$sql = "SELECT * FROM organization";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTC1(){
		$sql = "SELECT * FROM organization WHERE code_tc LIKE '%000000%' ";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTC2($code){
		$sql = "SELECT * FROM organization WHERE code_tc LIKE"."'".$code."%000' AND code_tc != "."'".$code."000000'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTC3($code){
		$sql = "SELECT * FROM organization WHERE code_tc LIKE"."'".$code."%' AND code_tc != "."'".$code."000'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectUserByCode($code){
		$sql = "SELECT * FROM user WHERE code=".$code;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectPageByUserID($user_id){
		$sql = "SELECT * FROM page WHERE user_id=".$user_id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectUserID($username){
		$sql = "SELECT * FROM user WHERE username='".$username."'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectPageLink($id){
		$sql = "SELECT * FROM auto_link WHERE page_link =".$id." ORDER BY position ASC ";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		// var_dump($sql);
		return $array;
	}
	public function selectTempGetKind($template){
		$sql = "SELECT * FROM template WHERE temp_path ='".$template."'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		// var_dump($sql);
		return $array;
	}
	public function selectFollowApprove(){
		$sql = "SELECT * FROM follow ";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTemplatePage(){
		$sql = "SELECT template FROM page ";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTemplatePageByID($id){
		$sql = "SELECT template FROM page WHERE id =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectFollowID($template){
		$sql = "SELECT follow_id FROM template where temp_path = '".$template."'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectFollowApproveID($id){
		$sql = "SELECT * FROM follow where id = '".$id."'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function getLinkPublic($kind){
		$sql = "SELECT page.id ,title,link,template,temp_path,kind FROM page INNER JOIN template on page.template = template.temp_path   WHERE kind = '".$kind."' AND status = '3'";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectPage(){
		$sql = "SELECT * FROM page ORDER BY id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectPageApproveC1($id){
		$sql = "SELECT * FROM page WHERE status = '4' AND user_id = ".$id." ORDER BY id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectPageApproveC2($id){
		$sql = "SELECT * FROM page WHERE status = '4'  AND user_id = ".$id." OR status = '5' AND user_id=".$id." ORDER BY user_id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectPageApproveC3($id){
		$sql = "SELECT * FROM page WHERE status = '4'  AND user_id = ".$id." OR status = '5' AND user_id=".$id." OR status = '6' AND user_id=".$id." ORDER BY user_id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}

	public function countPageApproveC2($id){
		$sql = "SELECT count(id) FROM page WHERE status = '4'  AND user_id = ".$id." OR status = '5' AND user_id=".$id." ORDER BY id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function countPageApproveC1($id){
		$sql = "SELECT count(id) FROM page WHERE status = '4' AND user_id = ".$id." ORDER BY id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}

	public function selectVersion($id){
		$sql = "SELECT * FROM version  WHERE page_id =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectVersionID($id){
		$sql = "SELECT * FROM version  WHERE id =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTest(){
		$sql = "SELECT * FROM test ORDER BY id DESC";
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTemplate(){
		$sql = "SELECT * FROM template";
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectTemplateByID($id){
		$sql = "SELECT * FROM template WHERE id =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function selectUser(){
		$sql = "SELECT * FROM user";
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}



	public function editPage($id){
		$sql = "SELECT title, content,status,new_path,template FROM page WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editArea($id){
		$sql = "SELECT * FROM area WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editBanner($id){
		$sql = "SELECT * FROM banner WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		// $array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function getBannerByID($id){
		$sql = "SELECT link_image FROM banner WHERE id_area =".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function editDepartment($code){
		$sql = "SELECT * FROM organization WHERE code_tc=".$code;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editDepartmentByID($id){
		$sql = "SELECT * FROM organization WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editVer($id){
		$sql = "SELECT title, content,ver,page_id FROM version WHERE page_id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editUser($id){
		$sql = "SELECT * FROM user WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editAutoLinkDepartment($id){
		$sql = "SELECT * FROM auto_link WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		$array = array();
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function editFollowApprove($id){
		$sql = "SELECT * FROM follow WHERE id=".$id;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array = $row;
		}
		return $array;
	}
	public function updatePage($id,$content,$title,$status){
		$sql = "UPDATE `page` 
                    SET 
                    `title`='".$title."',
                    `content`='".$content."',
                    `status`='".$status."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateArea($id,$name,$width,$height,$style,$number_sub,$list_id_banner){
		$sql = "UPDATE `area` 
                    SET 
                    `name`='".$name."',
                    `width`='".$width."',
                    `height`='".$height."',
                    `style`='".$style."',
                    `number_sub`='".$number_sub."',
                    `list_id_banner`='".$list_id_banner."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateBanner($id,$name,$image,$link_image,$id_area,$width,$height){
		$sql = 'UPDATE `banner` 
                    SET 
                    `name`="'.$name.'",
                    `image`="'.$image.'",
                    `link_image`="'.$link_image.'",
                    `id_area`="'.$id_area.'",
                    `width`="'.$width.'",
                    `height`="'.$height.'"
                    WHERE `id` = "'.$id.'"';
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateFollowApprove($id,$ar1,$ar2,$ar3,$name,$uploader,$master){
		$sql = "UPDATE `follow` 
                    SET 
                    `ar1`='".$ar1."',
                    `ar2`='".$ar2."',
                    `ar3`='".$ar3."',
                    `name`='".$name."',
                    `id_uploader`='".$uploader."',
                    `master`='".$master."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateDepartment($id,$name,$template){
		$sql = "UPDATE `organization` 
                    SET 
                    `name_tc`='".$name."',
                    `template_id`='".$template."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateAutoLink($id,$page_link,$name,$status,$kind,$dept_code){
		$sql = "UPDATE `auto_link` 
                    SET 
                    `page_link`='".$page_link."',
                    `name`='".$name."',
                    `status`='".$status."',
                    `kind`='".$kind."',
                    `dept_code`='".$dept_code."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateUser($id,$username,$password,$level,$code){
		$sql = "UPDATE `user` 
	                SET 
	                `username`='".$username."',
	                `password`='".$password."',
	                `level`='".$level."',
	                `code`='".$code."'
	                WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function UnPublish($id,$link,$status){
		$sql = "UPDATE `page` 
                    SET 
                    `link`='".$link."',
                    `status`='".$status."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateVersion($id,$content,$title){
		$sql = "UPDATE `page` 
                    SET 
                    `title`='".$title."',
                    `content`='".$content."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateStatus($id,$status){
		$sql = "UPDATE `page` 
                    SET 
                    `status`='".$status."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function deleteUser($id){
		$sql = "DELETE FROM user WHERE id=".$id;
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function deleteFollowApprove($id){
		$sql = "DELETE FROM follow WHERE id=".$id;
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function deletePage($id){
		$sql = "DELETE FROM page WHERE id=".$id;
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function deleteAutoLink($id){
		$sql = "DELETE FROM auto_link WHERE page_id=".$id;
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function deletePageLink($id){
		$sql = "DELETE FROM auto_link WHERE page_link=".$id;
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function deleteTemplate($id){
		$sql = "DELETE FROM template WHERE id=".$id;
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function updateLink($id,$link,$status){
		$sql = "UPDATE `page` 
                    SET 
                    `link`='".$link."',
                    `status`='".$status."'
                    WHERE `id` = '".$id."'";
       	$query = mysqli_query($this->connect->connection(),$sql);
       	return $query;
	}
	public function count(){
		$sql = "SELECT count(id) FROM page ";
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function countSearch($search){
		$sql = "SELECT count(id) FROM page WHERE title LIKE "."'%".$search."%'" ;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function countSearchStatus($search){
		$sql = "SELECT count(id) FROM page WHERE status LIKE "."'%".$search."%'" ;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function countSearchandStatus($search,$status){
		$sql = "SELECT count(id) FROM page WHERE title LIKE "."'%".$search."%' AND status LIKE "."'%".$status."%' " ;
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function paging($start, $limit){
		$sql = "SELECT * FROM page ORDER BY id DESC LIMIT ".$start.','.$limit;
		$array = array();
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function searchPage($search,$start,$limit){
		$sql = "SELECT * FROM page WHERE title LIKE "."'%".$search."%'"." ORDER BY id DESC  LIMIT ". $start.','.$limit;
	
		$array = array();
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function searchStatus($search,$start,$limit){
		$sql = "SELECT * FROM page WHERE status LIKE "."'%".$search."%'"." ORDER BY id DESC  LIMIT ". $start.','.$limit;
		$array = array();
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function SearchAndStatus($search,$status,$start,$limit){
		$sql = "SELECT * FROM page WHERE status LIKE "."'%".$status."%'"." AND  title LIKE "."'%".$search."%'"."  ORDER BY id DESC  LIMIT ". $start.','.$limit;
	   
		$array = array();
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	public function CountAll($search,$status,$start,$limit){
		$sql = "SELECT count(*) FROM page WHERE status LIKE "."'%".$status."%'"." AND  title LIKE "."'%".$search."%'"."  ORDER BY id DESC  LIMIT ". $start.','.$limit;
	    
		$array = array();
		$query = mysqli_query($this->connect->connection(),$sql);
		while($row = mysqli_fetch_assoc($query)){
			$array[] = $row;
		}
		return $array;
	}
	
}

 ?>

