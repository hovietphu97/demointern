<?php 
    ob_start();
	session_start();
	include_once dirname(__DIR__,2)."/common/header.php"  ;
	include_once dirname(__DIR__,2)."/config/ftp.php" ;
	include_once dirname(__DIR__,2)."/config/config.php" ;
  	include_once dirname(__DIR__,2)."/Models/page.php";
  	include_once dirname(__DIR__,2)."/Controller/PageController.php" ;
  	//echo dirname(__DIR__,2)."/config/ftp.php" ;
 ?>
 <?php
 	$ftp = new FTP();
	$con = $ftp->ftp_connection();
	$folder = ftp_nlist($con, '/public_html/feedback/csv');
	$page = new page();
	unset($folder[0]);
	unset($folder[1]);
	$count = 1;
	$number = 0;
	
	foreach ($folder as $key => $value_folder) {
		$arr_1 = array();
		$arr_2 = array();
		$folder_page_id = ftp_nlist($con, '/public_html/feedback/csv/'.$value_folder);
		$path = 'public_html/feedback/csv/'.$value_folder.'/feedback.csv';
		$local_path = dirname(__DIR__,2).'/Views/cron/';
		$local_file = $local_path.$value_folder.'/feedback.csv';
		$server_file = 'public_html/feedback/csv/'.$value_folder.'/feedback.csv';
		$server_path_id_page = 'public_html/feedback/csv/'.$value_folder;
		mkdir($local_path.$value_folder);
		$a = ftp_get($con, $local_file, $server_file, FTP_ASCII);
		$handle = fopen($local_file, 'r');
		while ($data= fgetcsv($handle, 100, ","))
		{
			list($fb1, $fb2) = $data;
			array_push($arr_1, $fb1);
			array_push($arr_2, $fb2);
			
		}
		fclose($handle);
		$count_arr_1 = array_count_values($arr_1);
		$count_arr_2 = array_count_values($arr_2);
		// var_dump($count_arr_1);
		foreach ($count_arr_1 as $key_count_arr_1 => $value_count_arr_1) {
			$page->insertAutoCron($value_folder,'1',$key_count_arr_1,$value_count_arr_1);
		}
		foreach ($count_arr_2 as $key_count_arr_2 => $value_count_arr_2) {
			$page->insertAutoCron($value_folder,'2',$key_count_arr_2,$value_count_arr_2);
		}
		unlink($local_file);
		rmdir($local_path.$value_folder);
		ftp_delete($con,$server_file);
		ftp_rmdir($con,$server_path_id_page);


	}
	header('location: http://demointern.com/feedback');

 ?>