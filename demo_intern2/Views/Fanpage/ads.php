<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  $page = new page();

  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Advertisement</h4>
    <form method = 'POST' style="color:white;">
    <div style="text-align: center; width: 50%; font-size: 30px;" class="form-group">
      <!-- <a style="color: red; padding-left: 10px;" href="/create-banner"><i class="fas fa-chart-area"></i></a> -->
      <a style="color: red; padding-left: 10px;" href="/create-area"><i class="fas fa-plus"></i></a>
      <button style="padding-left: 10px; border:none; background: none;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>
    </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Code</th>
          <th scope="col">View</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $listArea = $page->selectListArea();
        foreach($listArea as $value){
          $string = "<!-- Area id=".$value['id']."-->";
          $a = htmlentities($string);
      ?>
      <tbody>
        <tr>
          <td>
          <input type="checkbox" id = "checkbox" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
          </td>
          <td><?php echo $value['name']; ?></td>
          <td><?php echo $a; ?></td>
          <td>
            <button type="button" value="<?php echo $value['id']; ?>" class="btn btn-primary preview" data-toggle="modal" data-target=".bd-example-modal-lg1">
                <i class="fas fa-eye"></i>
            </button>
            <div class="modal fade bd-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body content-modal" id="content-modal">
                   
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </td>
          <td><a style="color:black;padding-left: 10px;" href="/edit-area?id=<?php echo $value['id'];?>"><i class="fas fa-edit"></i></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
       $(".preview").click(function(){
          var get_id = $(this).val();      
          $.ajax({
              type:'post',
              url:'/get-image',
              data:{get_id:get_id},
              success:function(content){
                $('.content-modal').html(content);
              }

          });
      });
        
     });
    </script>