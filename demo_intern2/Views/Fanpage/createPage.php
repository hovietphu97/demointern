<?php session_start(); 
//dirname(__DIR__) : dùng để lui về thư mục;
?>

<?php
 	include_once dirname(__DIR__,2)."\common\header.php"  ;

	include_once dirname(__DIR__,2)."\config\config.php" ;

  include_once dirname(__DIR__,2)."\Models\page.php" ;

  include_once dirname(__DIR__,2)."\Controller\PageController.php" ;

?>
<?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>

  <?php
    $page = new page();
    $dir  = dirname(__DIR__,2).$_POST['optradio'];
    if(isset($_POST['submit_temp'])){
        $dir = dirname(__DIR__,2).$_POST['editTemp'];
    }
    $original = file_get_contents($dir);
   	$partern_title = "~<h3>([^<]*)<\/h3>~";
  	$subject_title = file_get_contents($dir);
  	$replace_title = "<h3 class='title_h3' style='margin-right:20px; display:inline-block;'>".$_POST['title']."</h3>"."<button style='border:none; background:none;font-size:25px;' class='edit'><i class='fas fa-edit'></i></button>"."<input value='".$_POST['title']."' type = 'hidden' class='title_edit' name = 'title'>"."<input type='hidden' name ='newPath' value = '".$_POST['newPath']."'>"."<input type='hidden' name ='optradio' value = '".$_POST['optradio']."'>";
  	file_put_contents($dir, preg_replace($partern_title, $replace_title, $subject_title));
    $a = file_get_contents($dir);
 
  	file_put_contents($dir, $original);
   
  	if(isset($_POST['submit'])){

	    $add = new PageController();
	    $content = $_POST['content'];
      $page = new page();
      $edit_user_id = $page ->editUser($_SESSION['user_id']);
        $title = trim(htmlspecialchars($_POST['title'])) ;
      	$newPath = '';
      	if(!empty($_POST['newPath'])){
          $newPath = $_POST['newPath'];
        }
        $status ='1';
        if($edit_user_id['level'] == 2){
          $status ='4';
        }
        $template = $_POST['optradio'];
	       
	     $result = $add->create($title, $content,$status,$newPath,$template,$_SESSION['user_id']);
	    if($result){
	      header("Location:".HOST.SUCCESS_PATH.$success);
	    }else{
	      header("Location:".HOST.FAIL_PATH.$fail);
	    }            

  	}
  	$temp = new page();
  	$getTemp = $temp->selectTemplate();
//   	echo "<pre>";
//   	var_dump($getTemp);
?>
<div>
   
	<div class="container" style="padding: 0;">
	   <!--  <div class='edit_page' style='float:right;'>-->
    <!--    aaa-->
    <!--</div>-->
		<form method="POST" >
			<?php echo $a; ?>

			<div class="form-group">
			    <input style="display: inline-block; width: 50%; margin-bottom:15px;" disabled type="text" name ='newPath' value = "<?php if(!empty($_POST['newPath'])){ echo $_POST['newPath']; } ?>" id ='newPath' class="form-control"><br>
		      	<input type="hidden" name="status" class="form-control" id="status" disabled value="New ">
			    
			    <select style='width:14%; display:inline-block;' class = 'form-control editTemp' name = 'editTemp'>
			         <option value = ''>Select Template</option>
			    <?php
			    
			        foreach($getTemp as $valueTemp){
			     ?>
			     
			        
			         <option value = <?php echo $valueTemp['temp_path'];?>><?php echo $valueTemp['temp_name']; ?></option>
			    
			        <?php
			            }
    			    ?>
			     </select>
			     <button type="submit" id='submit_temp' style='margin-bottom:5px;' name="submit_temp" class="btn btn-primary">Edit Template</button>
			     <button type="submit" style='display:block; margin-top:11px;' id='submit' name="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
<script>
    // Thay thế <textarea id="post_content"> với CKEditor
    CKEDITOR.replace( 'content' , {
      filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
      filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
     });// tham số là biến name của textarea
   
</script>
<script type="text/javascript">
 $(document).ready(function(){
    $("#submit").click(function(){
        var content = CKEDITOR.instances['content'].getData();
        var title = $('.title_edit').val();
        var flag = true;
        console.log(content);
        if(content == ''){
            alert('xin mời nhập nội dung');
            flag = false;
        }else if(title != ''){
                
                $.ajax({
                    async: false,
                  type:'post',
                  url:'/validate-title',
                  data:{title:title},
                  success:function(respone){
                      console.log(respone);
                      if(respone == 1){
                            alert('title đã tồn tại');
                            flag = false;
                      }
                      
                    }
                });
              
            }
        return flag;
    });
    $(".edit").click(function(){
        console.log(1);
        $('.title_h3').hide();
        $('.title_edit').attr('type','text');
        $(this).hide();
        return false;
    });
    
    $("#submit_temp").click(function(){
        var temp = $('.editTemp').val();
        console.log(1);
        if(temp == ''){
            alert('Vui lòng chọn template cần đổi');
            return false;
        }
       
    });
    
 });
 
</script>