<?php 
	session_start();
	include_once dirname(__DIR__,2)."\common\header.php"  ;
  include_once dirname(__DIR__,2)."\Models\page.php";
  include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
  include_once dirname(__DIR__,2)."\config\config.php";
?>
<?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
<?php
  if(!empty($_GET['id'])){
    $id = $_GET['id'];
    $edit = new PageController();
    $editID  = $edit->edit($id);
    
  }
?>
<?php
      $list = new page();
      $editPage = $list->editPage($id);
      
      if($editPage['template'] != ''){

        $value_temp = $editPage['template'];
        $path = "https://publicgdit.000webhostapp.com";
        $dir_get_link = dirname(__DIR__,2).'/template/temp.txt';
        
        $abc = '';
        $original_link = file_get_contents($dir_get_link);
       
        $dir  = dirname(__DIR__,2).$editPage['template'];
        $original = file_get_contents($dir);
        $partern_title = "~<h3>([^<]*)<\/h3>~";
        $subject_title = file_get_contents($dir);
        $replace_title = "<label>Title:</label><br><input id = 'title' style='width: 100%;' value='".$editPage['title']."' type = 'text' name = 'title'><input id = 'get_id' style='width: 100%;' value='".$_GET['id']."' type = 'hidden' name = 'get_id'><br>";
        file_put_contents($dir, preg_replace($partern_title, $replace_title, $subject_title));
        
        $partern_content = "~<textarea[^>]*>[^<]*</textarea>~";
        $subject_content = file_get_contents($dir);
        $replace_content = "<label>Content:</label><br><textarea type='text' name='content' class='form-control' id='content' rows='10' cols='150' value = '".$editPage['content']."' >".$editPage['content']."</textarea>";
        file_put_contents($dir, preg_replace($partern_content, $replace_content, $subject_content));
        
          $a = file_get_contents($dir);
         
          file_put_contents($dir, $original);
    }
?>
<?php
  if(isset($_POST['submit'])){

    $update = new PageController() ;
    
    if(isset($_POST['title'])){
      $title = trim(htmlspecialchars($_POST['title']));
    }
    if(isset($_POST['content'])){
      $content = $_POST['content'];
    }
    if($editPage['title'] != $_POST['title'] || $editPage['content'] != $_POST['content'] ){

      $page_id = $_GET['id'];
      $editVersion = $list->editVer($page_id);
    
      if($editVersion['ver'] != null){
        $a  = preg_split ("/\ /", $editVersion['ver']); 
        $b = (int)$a[1] +1;
        $ver = $a[0].' '.$b;
      }else{
        $ver = 'Version 1';
      }
    
      $verTitle = $editPage['title'];
      $verContent = $editPage['content'];
      $version = $update->createVer($verTitle, $verContent,$ver,$page_id);

    }
    if(isset($_GET['id'])){
      $id = $_GET['id'];
    }

    if($editPage['status'] == '1'){
      $status = $_POST['status'] = '1';
    }
     if($editPage['status'] == '3'){
      $status = $_POST['status'] = '2';
    }
    if($editPage['status'] == '2'){
      $status = $_POST['status'] = '2';
    }
    if($editPage['status'] == '0'){
      $status = $_POST['status'] = '2';
    }
    $page = new page();
    $edit_user_id = $page ->editUser($_SESSION['user_id']);
    if($edit_user_id['level'] == 2){
          $status ='4';
    }
   
    $updateInfo = $update->update($id,$content,$title,$status);
   
    if($updateInfo){
      //$_SESSION['success'] = $title;
      header("Location:".HOST.LIST_PATH);
    }else{
      //$_SESSION['fail'] = $title;
      header("Location:".HOST.LIST_PATH);
    }
  }

?>
 <style type="text/css">
  div.login{
    width: 40%;
    margin: 0 auto;
  }
</style>

<form action ="" method="POST">
    <?php echo $a; ?>
 
  <div class='container'>
  <button type="submit" name='submit' id = 'submit' style= 'color:white; background:blue;' class="btn btn-default submit">Update</button>
  </div>
</form>


<script>
    // Thay thế <textarea id="post_content"> với CKEditor
    CKEDITOR.replace( 'content' , {
      filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
      filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
     });// tham số là biến name của textarea
    $(document).ready(function(){
        $("#submit").click(function(){
          var title = ($("#title").val()).trim();
          var content = CKEDITOR.instances['content'].getData();
          var get_id = $('#get_id').val();
          var flag = true;
          if(title.length > 50){
            $('.title-error').text("title quá 50 kí tự");
            flag = false;
          }
          
         if(title == ""){
           alert('xin mời nhập title');
            flag = false;
          }else if(content == ""){
           alert('xin mời nhập content');
            flag = false;
          }else if(title != ''){
                
                $.ajax({
                    async: false,
                    type:'post',
                    url:'/validate-edit-title',
                    data:{title:title,get_id:get_id},
                    success:function(respone){
                        if(respone == 1){
                            alert('title đã tồn tại');
                            flag = false;
                        }
                      
                    }
                });
              
            }
          return flag;
   
        });
    });

</script>


