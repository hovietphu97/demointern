<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  $page = new page();
  if(isset($_POST['remove'])){
    if(isset($_POST['checkbox'])){
      foreach ($_POST['checkbox'] as $key => $value) {
        $delete = $page->deleteFollowApprove($value);
      }
    }
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">LIST FOLLOW APPROVE</h4>
    <form method = 'POST'>
    <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
    <a style="color: red; padding-left: 10px;" href="/create-follow-approve"><i class="fas fa-plus"></i></a>
    <button style="padding-left: 10px; border:none; background: none;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Follow</th>
          <th scope="col">ID Uploader</th>
          <th scope="col">Master</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $page = new page();
        $listTemp = $page->selectFollowApprove();
        foreach($listTemp as $value){
          $ar1 = '';
          $ar2 = '';
          $ar3 = '';
          $char_ar1 = '';
          $char_ar2 = '';
          $char_ar3 = '';
          if($value['ar1'] == 1){
            $ar1 = 'AR1';
            $char_ar1 = '->';
          }
          if($value['ar2'] == 1){
            $ar2 = 'AR2';
            $char_ar2 = '->';
          }
          if($value['ar3'] == 1){
            $ar3 = 'AR3';
            $char_ar3 = '->';
          }
      ?>
      <tbody >
        <tr>
          <td>
          <input type="checkbox" id = "checkbox" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
          </td>
          <td><?php echo $value['name'] ?></td>
          <td>
            <?php
              echo $ar1.$char_ar1.$ar2.$char_ar2.$ar3.$char_ar3.'MASTER';
            ?>
          </td>
          <td>
            <?php
              echo $value['id_uploader'];
            ?>
          </td>
          <td>
            <?php
              if($value['master'] == 1){
                echo "Allow";
              }else{
                echo "Don't Allow";
              }
            ?>
          </td>
          <td>
            <a style="color:black;padding-left: 10px;" href="/edit-follow-approve?id=<?php echo $value['id'];?>"><i class="fas fa-edit"></i></a>
          </td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        
     });
    </script>