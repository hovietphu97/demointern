<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
    include_once dirname(__DIR__,2)."\config\database.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }

 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">CSV FILE</h4>
    <form method = 'POST' action="/export" enctype="multipart/form-data">
	    <div style="margin:0 auto; width: 60%; font-size: 30px;" class="form-group">
	    	<button style="padding-left: 10px;background: blue; color: white" type="submit" name="export" class="btn btn-primary export" id ="export">Export</button>
	    </div>
    </form>
    <form method = 'POST' action="/import" enctype="multipart/form-data">
	    <div style="margin:0 auto; margin-top: 15px; width: 60%; font-size: 18px;" class="form-group">
		    <button style="padding-left: 10px;background: red; color: white" type="submit" name="import" class="btn btn-primary import" id ="import">Import</button>
		    <input type="file" name="file" class = 'file' >
		</div>
	</form>
<script type="text/javascript">
	$(document).ready(function(){

		$(".export").click(function(){
		 	if(confirm('Are you sure?')){
            	return true;
	        }else{
	            return false;
	        }
		});

		$(".import").click(function(){
		 	var filezip = $('.file').val();
		 	if(filezip == ''){
		 		alert('chưa chọn tệp ');
              	return false;
		 	}
		 	return true;
		});
	});
</script>