<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\Controller\PageController.php";
 ?>

 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  if(isset($_POST['import'])){
  	$controller = new PageController();
  	 $filename=$_FILES["file"]["tmp_name"];
  	 // $file = fopen($filename, "r");
  	 // $getData = fgetcsv($file, 10000, ",");
  	 // var_dump($getData);
  	 // exit;
  	 if($_FILES["file"]["size"] > 0)
     {
     	$file = fopen($filename, "r");
	    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
	    {
			$insert_user = $controller->createUser($getData[0],$getData[1],$getData[2],$getData[3]);
	    }
	    fclose($file);

	    if($insert_user){
	      header("Location:".HOST.'/list-user');
	    }else{
	      header("Location:".HOST.'/csv');
	    }  
     }
  }
 ?>