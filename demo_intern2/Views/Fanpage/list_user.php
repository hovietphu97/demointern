<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  $page = new page();

  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  if(isset($_POST['remove'])){
    foreach ($_POST['checkbox'] as $key => $value) {
      $delete = $page->deleteUser($value);
    }
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">LIST USER</h4>
    <form method = 'POST' style="color:white;">
      <div style="text-align: center; width: 50%; font-size: 30px;" class="form-group">
      <a style="color: red; padding-left: 10px;" href="/create-user"><i class="fas fa-plus"></i></a>
      <button style="padding-left: 10px; border:none; background: #fff;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>
    <!-- <a style="color: red; padding-left: 10px;" href="/organization"><i class="fas fa-sitemap"></i></a>
    <a style="color: green; padding-left: 10px;" href="/list-department"><i class="fas fa-list"></i></a>
    <a style="color: green; padding-left: 10px;" href="/csv"><i class="fas fa-file-import"></i></a> -->
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Password</th>
          <th scope="col">Level</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $listUser = $page->selectUser();
        foreach($listUser as $value){
      ?>
      <tbody>
        <tr>
          <td>
          <input type="checkbox" id = "checkbox" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
          </td>
          <td><?php echo $value['username'] ?></td>
          <td><?php echo $value['password'] ?></td>
          <td><?php echo $value['level'] ?></td>
          <td><a style="color:black;padding-left: 10px;" href="/edit-user?id=<?php echo $value['id'];?>"><i class="fas fa-edit"></i></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        
     });
    </script>