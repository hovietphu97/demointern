<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
 <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
    $page = new page();
    $getTC1 = $page->selectTC1();
    $edit_autolink = $page->editAutoLinkDepartment($_GET['id']);
    $sub_str_tc1 = substr($edit_autolink['dept_code'], 0,3);
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Auto Link</h4>
    <form method = 'POST' id='form_submit' action="/update-page-autolink-department?id=<?php echo $_GET['id']; ?>" >
      
      <div style="text-align: left; width: 50%; margin:0 auto;" class="form-group">
      <div class="form-group">
        <label style="display: inline-block;" class="control-label col-sm-3" >Name AutoLink:</label>
        <input style="display: inline-block; width:30%;" name="name_auto_link"  class="form-control name_auto_link" id="name_auto_link" value="<?php echo $edit_autolink['name']; ?>">
      </div>
      <div class="form-group">
        <label style="display: inline-block;" class="control-label col-sm-3" >Status:</label>

        <input style="display: inline-block; width:2%;" onchange="department()" type="radio" name="status" class="form-control" id="kind_temp" <?php if($edit_autolink['status'] == 1){echo "checked";} ?> value="1" >
        <label class="control-label col-sm-3">Template</label>

        <input style="display: inline-block; width:2%;" onchange="department()" type="radio" name="status" <?php if($edit_autolink['status'] == 2){echo "checked";} ?>  class="form-control " id="dept_code" value="2" >
        <label class="control-label col-sm-3">Code</label>

      </div>

      <div style="display: none;" class="form-group category_template">
        <label style="display: inline-block;" class="control-label col-sm-3">Category template</label>
        <select style="width: 20%;display: inline-block;" class="form-control category" onchange="listPage()" name='category'>
          <option value="">---</option>
          <option value="1" <?php if($edit_autolink['kind'] == 1){ echo "selected";} ?> >Du lịch</option>
          <option <?php if($edit_autolink['kind'] == 2){ echo "selected";} ?> value="2">Xã hội</option>
          <option <?php if($edit_autolink['kind'] == 3){ echo "selected";} ?> value="3">Cảnh vật</option>
        </select>
      </div>
      <div style="display: none;" class="form-group related_page">
        <label style="font-weight: bold;"  class="control-label col-sm-3">Related Page:</label>
        <table style = "width: 85%;" class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Page</th>
              <th scope="col">Link</th>
            </tr>
          </thead>
          <tbody id="value_related_page">
              
          </tbody>
        </table>
      </div>

      <div style="display: none;"  class="form-group department">
        <select style="display: inline-block; margin-left: 14px; width:25%;" onchange="code1()"  class="form-control col-sm-4" id ='tc_c1' name="tc_c1" >
            <option value="">---</option>
            <?php 
              foreach ($getTC1 as $key => $value) {
                $a = substr($value['code_tc'],0,3);
             ?>
              <option value="<?php echo $a ?>" <?php if($a == $sub_str_tc1){echo "selected";} ?>><?php echo $value['name_tc']; ?></option>
            <?php
              }
            ?>
          </select>
          <select  style="display: inline-block; width:25%; margin: 0 7px 0 7px;" onchange="code2();"  class="form-control col-sm-4" name="tc_c2" id='tc_c2' >
              
          </select>
          <select  style="display: inline-block; width:25%;"  onchange="getPageDepartment()" class="form-control col-sm-4" name="tc_c3" id='tc_c3' >
              
          </select>
      </div>
      <div style="display: none;" class="form-group related_page_code">
        <label style="font-weight: bold;"  class="control-label col-sm-3">Related Page:</label>
        <table style = "width: 85%;" class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Page</th>
              <th scope="col">Link</th>
            </tr>
          </thead>
          <tbody id="value_related_page_code">
              
          </tbody>
        </table>
      </div>

      <div class="form-group">
        <button type="button" class='btn btn-primary button' onclick="submitForm()" style="color: white; background-color:blue; " >Update</button> 
      </div>
    </div>
    </form>
<script type="text/javascript">

    function code1 (){
        var value_code1 = $('#tc_c1').val();
        if(value_code1 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-tc2',
              data:{value_code1:value_code1},
              success:function(response){
                $('#tc_c2').html(response);
              } 
          });

          $('#code_1').hide();
          $('#code_2').attr('style','width: 10%; display: inline-block;');
          $('#lb_code_2').hide();
          $('#lb_code_1').attr('style','width: 10%; display: inline-block; border: none;');
          $('#lb_code_1').html(value_code1);
        }else{
          $('#code_1').show();
          $('#code_2').attr('style','width: 10%; display: none;');
          $('#code_3').attr('style','width: 10%; display: none;');
          $('#lb_code_2').show();
          $('#lb_code_2').html('000');
          $('#lb_code_3').show();
          $('#lb_code_3').html('000');
          $('#lb_code_1').hide();
          $('#tc_c2').html("<option value = '' >---</option>");
        }
        return false;
      }

      var value_code1 = $('#tc_c1').val();
      var get_id = "<?php echo $_GET['id']; ?>" ; 
        if(value_code1 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-edit-tc2',
              data:{value_code1:value_code1,get_id:get_id},
              success:function(response){
                $('#tc_c2').html(response);
                var value_code2 = $('#tc_c2').val();
                $.ajax({
                    type:'post',
                    url:'/get-value-edit-tc3',
                    data:{value_code1:value_code1,value_code2:value_code2,get_id:get_id},
                    success:function(response){
                      $('#tc_c3').html(response);
                      var value_code3 = $('#tc_c3').val();
                      $.ajax({
                        url: '/get-page-edit-department',
                        type: 'POST',
                        data:{value_code1:value_code1,value_code2:value_code2,value_code3:value_code3,get_id:get_id},
                        success: function(response){
                          $('#value_related_page_code').html(response);
                        }
                      });
                    } 
                });

              } 
          });
        }

          

      function code2 (){
        var value_code2 = $('#tc_c2').val();
        var value_code1 = $('#tc_c1').val();
        if(value_code2 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-tc3',
              data:{value_code1:value_code1,value_code2:value_code2},
              success:function(response){
                $('#tc_c3').html(response);
              } 
          });
          $('#code_2').hide();
          $('#code_3').attr('style','width: 10%; display: inline-block;');
          $('#lb_code_3').hide();
          $('#lb_code_2').attr('style','width: 10%; display: inline-block; border: none;');
          $('#lb_code_2').html(value_code2);
        }else{
          $('#code_2').show();
          $('#code_3').attr('style','width: 10%; display: none;');
          $('#lb_code_3').show();
          $('#lb_code_2').hide();
        }
        return false;
      }

      function department(){
        var template = $('#kind_temp').prop("checked");
        var code = $('#dept_code').prop("checked");

        if(template == true){
          $('.category_template').show();
          $('.related_page').show();
          $('.department').attr('style','display:none');
          $('.related_page_code').attr('style','display:none');
        }
        if(code == true){
          $('.department').show();
          $('.related_page_code').show();
          $('.category_template').attr('style','display:none');
          $('.related_page').attr('style','display:none');
        }
      }
      var template = $('#kind_temp').prop("checked");
      var code = $('#dept_code').prop("checked");
      if(template == true){
        $('.category_template').show();
        $('.related_page').show();
        $('.department').attr('style','display:none');
        $('.related_page_code').attr('style','display:none');
      }
      if(code == true){
        $('.department').show();
        $('.related_page_code').show();
        $('.category_template').attr('style','display:none');
        $('.related_page').attr('style','display:none');
      }

      function getPageDepartment(){
        var value_code2 = $('#tc_c2').val();
        var value_code1 = $('#tc_c1').val();
        var value_code3 = $('#tc_c3').val();
        $.ajax({
              url: '/get-Page-Department',
              type: 'POST',
              data:{value_code1:value_code1,value_code2:value_code2,value_code3:value_code3},
              success: function(response){
                $('#value_related_page_code').html(response);
              }
          });

      }

      function listPage(){
        var category = $('.category').val();
        var link_page = "<?php echo $edit_autolink['page_link']; ?>";
        if(category != ''){
          $.ajax({
              url: '/get-page-edit-autolink-department',
              type: 'POST',
              data:{category:category,link_page:link_page},
              success: function(response){
                $('#value_related_page').html(response);
              }
          });
        }
      }
     var category = $('.category').val();
        var link_page = "<?php echo $edit_autolink['page_link']; ?>";
        if(category != ''){
          $.ajax({
              url: '/get-page-edit-autolink-department',
              type: 'POST',
              data:{category:category,link_page:link_page},
              success: function(response){
                $('#value_related_page').html(response);
              }
          });
        }
      function submitForm (){
          var page_link = [];
          var flag = true;
          var checkbox = $('.checkbox:checked').val();
          var boxes = $('input[name="checkbox[]"]:checked');
          boxes.each(function(i,v){
            page_link.push($(v).val());
          })
          if(!checkbox){
            alert('Please check');
            flag = false;
          }
          if(flag == true){
            $('#form_submit').submit();
          }

      }

</script>  
