<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  $page = new page();
  if(isset($_POST['remove'])){
    if(isset($_POST['checkbox'])){
      foreach ($_POST['checkbox'] as $key => $value) {
        $delete = $page->deleteTemplate($value);
      }
    }
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">LIST TEMPLATE</h4>
    <form method = 'POST'>
    <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
      <a style="color: red; padding-left: 10px;" href="/create-template"><i class="fas fa-plus"></i></a> 
      <button style="padding-left: 10px; border:none; background: #fff;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>
    <!-- <a style="color: red; padding-left: 10px;" href="/create-follow-approve"><i class="fas fa-flag"></i></a> -->
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Path</th>
          <th scope="col">Follow</th>

        </tr>
      </thead>
      <?php
        $page = new page();
        $listTemp = $page->selectTemplate();
        foreach($listTemp as $value){
      ?>
      <tbody>
        <tr>
          <td>
          <input type="checkbox" id = "checkbox" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
          </td>
          <td><?php echo $value['temp_name'] ?></td>
          <td><?php echo $value['temp_path'] ?></td>
          <td><?php echo $value['follow_id'] ?></td> 
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        
     });
    </script>