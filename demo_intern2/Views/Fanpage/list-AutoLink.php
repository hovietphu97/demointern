<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
 <?php
        
        $page = new page();
        $Controller = new PageController();
        if(isset($_POST['submit'])){
          $id_update = $_POST['edit_position_hide'];
          if(isset($_POST['edit_page_link'])){
            $id_page_link_update = $_POST['edit_page_link'];
            // var_dump($id_page_link_update);

          }
          $count = 0;
          $test = [];
          foreach ($id_update as $key => $value_update) {
            // if(!in_array($value_update, $test)){
            //   $test[] = $value_update;
              $update_position = $Controller->updateAutoLink($value_update, $id_page_link_update[$count]);
              $count ++;
            // }else{
            //   echo '<input class="fail" value="fail" >';
            // }
            // var_dump($id_page_link_update);
            
          }
        }

      ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">List Autolink</h4>
    <form id='frm_submit' method = 'POST' >
      <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
        <button style='border:none; display: inline-block; background:none;font-size:25px;' class='edit' ><i class='fas fa-edit'></i></button>
        <button style='border:none; display: none; background:none;font-size:25px;'  class='submit' type="submit" name='submit' ><i class="fas fa-check-circle"></i></i></button>
      </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Page Link</th>
          <th scope="col">Position</th>
        </tr>
      </thead>
      <?php 
          $getAutolink = $page ->selectAutoLink($_GET['id']);
          foreach ($getAutolink as $key => $value) {

       ?>
      <tbody>
        <tr>
          <td><input type="checkbox" name="checkbox[]" class="checkbox"></td>
         <td><?php echo $value['page_link']; ?></td>
         <td>
          <p class = 'edit_position'style='display: inline-block;' ><?php echo $value['position']; ?></p>
          <input type="hidden"   name="edit_position_hide[]" min ='1' max = '100' class='edit_position_hide' value="<?php echo $value['position']; ?>">
          <input type="hidden" name="edit_page_link[]"  class='edit_page_link' value="<?php echo $value['page_link']; ?>">
         </td>
        </tr>
      </tbody>
     <?php
          }
      ?>
    </table>

    </form>
<script type="text/javascript">

  var arr = [];
 

    $(document).ready(function(){
      $(".edit").click(function(){
        $('.edit_position').hide();
        $(this).hide();
        $('.submit').show();
        $('.edit_position_hide').attr('type','number')
        return false;
      });

      $('.submit').click(function(){
        var inputs = $(".edit_position_hide");
        var position = [];
        var flag = true;
        var id_page = <?php echo $_GET['id']; ?>;
        for(var i = 0; i < inputs.length; i++){
          var value = $(inputs[i]).val();
          if(value == ''){
            alert('Position không được bỏ trống');
            flag = false;
          }else if(value == 0){
            alert('Position bắt đầu từ vị trí số 1')
              flag = false;
          }
          position.push(value);
          var test = [];
          position.forEach(function(val, index){
              if(!test.includes(val)){
                test.push(val);
                flag = true;
              }else{
                alert('false');
                flag =  false;
              }
          })
        }
          // if (flag == true) {
          //   $('#frm_submit').submit();
          // }
          // console.log(flag);
        return flag;
       });


    });

 
</script>