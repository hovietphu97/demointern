<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\Models\page.php";
    
 ?>

 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
	  	$filename = 'users.csv';
	  	$page = new page();
	  	$list_user = $page ->selectUser();
		// file creation
		$file = fopen($filename,"w");
		fputcsv($file,array('ID','Username','Password','Level','Code'));
		foreach ($list_user as $key => $value) {
			fputcsv($file, $value);
		}
		fclose($file);
		// download
		header("Content-Disposition: attachment; filename=".$filename);
		header("Content-Type: text/csv; "); 
		readfile($filename);
		unlink($filename);
 ?>