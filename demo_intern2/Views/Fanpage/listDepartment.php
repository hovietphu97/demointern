<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">LIST DEPARTMENT</h4>
    <form method = 'POST'>
      <div style="text-align: center; width: 47%; font-size: 30px;" class="form-group">
      <a style="color: red; padding-left: 10px;" href="/organization"><i class="fas fa-plus"></i></a>
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Tên tổ chức</th>
          <th scope="col">Code</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $page = new page();
        $listUser = $page->selectOrganization();
        foreach($listUser as $value){
      ?>
      <tbody>
        <tr>
          <td>
              <?php echo $value['id']; ?>
          </td>
          <td><?php echo $value['name_tc'] ?></td>
          <td><?php echo $value['code_tc'] ?></td>
          <td><a href="/edit-department?id=<?php echo $value['id']; ?>"><i class="fas fa-edit"></i></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    