<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">LIST FEEDBACK</h4>
    <form method = 'POST'>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">ID Page</th>
          <th scope="col">Question</th>
          <th scope="col">Answer : Number of</th>
        </tr>
      </thead>
      <?php
        $page = new page();
        $auto_cron = $page -> selectCronFeeback();
        foreach ($auto_cron as $key => $value) {
          $substr_replace = substr_replace($value['demo'], ' ', 1,0);
          $substr_replace_2 = substr_replace($substr_replace, ' ',3,0);
          $substr_replace_3 = substr_replace($substr_replace_2, ' ', 7,0);
          $substr_replace_4 = substr_replace($substr_replace_3, ' ',9,0);
          $substr_replace_5 = substr_replace($substr_replace_4, ' ', 13,0);
          $substr_replace_6 = substr_replace($substr_replace_5, ' ', 15,0);
          $substr_char = substr_replace($substr_replace_6, ' ', 5,0);
          $substr_char_2 = substr_replace($substr_char, ' ', 7,0);
          $substr_char_3 = substr_replace($substr_char_2, ' ', 13,0);
          $substr_char_4 = substr_replace($substr_char_3, ' ', 15,0);
      ?>
      <tbody >
        <tr>
          <td><?php echo $value['page_id']; ?></td>
          <td><?php echo $value['question']; ?></td>
          <td><?php echo $substr_char_4; ?></td>
        </tr>
      </tbody>
    <?php } ?>
    </table>
    </form>
