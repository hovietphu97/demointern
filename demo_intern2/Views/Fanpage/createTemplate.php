<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  if(isset($_POST['upload'])){
        $temp = new PageController();
        
        $path = dirname(__DIR__,2).'/template';
        $a = pathinfo($_FILES['file_zip']['name']);
        
        $b =  $path.'/'.$a['filename'];
        // $b = $_FILES['file_zip']['tmp_name'] ;
        $move = $_FILES['file_html']['tmp_name'];
        
        $zip = new ZipArchive;
        if ($zip->open($_FILES['file_zip']['tmp_name']) === TRUE) {
            $zip->extractTo($path);
            $zip->close();
            echo 'ok';
        } else {
            echo 'failed';
        }
        move_uploaded_file($move, $path.'/'.$a['filename'].'/'.$_POST['name_template'].'.html');
        $path_temp = '/template/'.$a['filename'].'/'.$_POST['name_template'].'.html';
        $name = $_POST['name_template'];
        $kind = $_POST['category'];
        $follow_id = $_POST['follow'];
        $createTemplate = $temp->createTemp($name,$path_temp,$kind,$follow_id);
        
        if($createTemplate){
            header("Location:".HOST.'/list-template');
        }else{
            header("Location:".HOST.'/create-template');
            echo "Tạo template thất bại !!! ";
        }
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">CREATE TEMPLATE</h4>
    <form method="POST" enctype="multipart/form-data" style='width: 50%; margin: 0 auto;' >
      <div style="font-size:30px;" class="form-group">
          <a href="/list-template"><i class="fas fa-list"></i></a>
      </div>
      <div class="form-group">
        <label>Name Template</label>
        <input type="text" name = 'name_template' class="form-control name_temp" placeholder="Enter name template">
       
      </div>
      <div class="form-group">
        <label >File html</label>
        <input type="file" class="form-control-file file_html" name ="file_html">
         <p class ='html_error'></p>
      </div>
       <div class="form-group">
        <label>File Zip</label>
        <input type="file" class="form-control-file file_zip" name ="file_zip">
         <p class ='zip_error'></p>
      </div>
      <div class="form-group">
        <label>Category template</label>
        <select style="width: 20%;" class="form-control category" name='category'>
          <option value="1">Du lịch</option>
          <option value="2">Xã hội</option>
          <option value="3">Cảnh vật</option>
        </select>
      </div>
      <div class="form-group">
        <label>Follow Approve</label>
        <select style="width: 40%;" class="form-control follow" name='follow'>
          <?php
            $page = new page();
            $select_follow_approve = $page->selectFollowApprove();
            foreach ($select_follow_approve as $key => $value) {
          ?>
          <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
          <?php } ?>
        </select>
      </div>
       <div class="form-group">
        <a href = 'https://drive.google.com/file/d/1DUYpL_JS7M5C2rYpXUQQu1ylQdjK5DmS/view' target = 'blank'>Click vào đây để xem hướng dẫn tạo template</a>
      </div>
      <button type="submit" class="btn btn-primary submit" name='upload'>Submit</button>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".submit").click(function(){
          var name = $('.name_temp').val();
          var filehtml = $('.file_html').val();
          var filezip = $('.file_zip').val();
          var format = /[!@#$%^&*(),.?":{}|<>]/;
          
        //   if(format.test(name)){
        //     alert('không được dùng kí tự đặc biệt');
        //     return false;
        //   }
          if(name == ''){
              alert('chưa nhập tên template');
              return false;
          }else if(format.test(name)){
            alert('không được dùng kí tự đặc biệt');
            return false;
          }else if(filehtml == ''){
              alert('chưa chọn file template');
              return false;
          }else if(filezip == ''){
              alert('chưa chọn tệp template');
              return false;
          }
          if(filehtml.includes('.html') == false){
              $('.html_error').text('file html không đúng định dạng');
              return false;
          }else{
              $('.html_error').text('');
          }
          if(filezip.includes('.zip') == false){
              $('.zip_error').text('file zip không đúng định dạng');
              return false;
          }else{
              $('.zip_error').text('');
          }
          
          return true;
        });
        
     });
    </script>