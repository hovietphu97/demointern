<?php 
    ob_start();
	session_start();
	include_once dirname(__DIR__,2)."/common/header.php"  ;
	include_once dirname(__DIR__,2)."/config/ftp.php" ;
	include_once dirname(__DIR__,2)."/config/config.php" ;
  	include_once dirname(__DIR__,2)."/Models/page.php";
  	include_once dirname(__DIR__,2)."/Controller/PageController.php" ;
  	//echo dirname(__DIR__,2)."/config/ftp.php" ;
 ?>

<?php
	
	if(isset($_SESSION['checkbox'])){
		
		$info = new page();
		$ftp = new FTP();
		$updateLink = new PageController();
		$con = $ftp->ftp_connection();
		
		foreach ($_SESSION['checkbox'] as $key => $value_id){
		    $getInfo = $info->editPage($value_id);
		    $path = dirname(__DIR__,2).$getInfo['template'];
    		$original = file_get_contents($path);
			$dir_img = 'public_html/images';
			//xem file trong thu muc
			$folder = ftp_nlist($con, '/public_html');
           	$new_dir_img = '';
			foreach ($folder as $key => $link_folder) {
				// var_dump($link_folder);
				if($link_folder == $dir_img){
					$new_dir_img = true;
				}
			}
			//kiem tra neu khong co thi tao, con co roi thi thoi
			if($new_dir_img != ''){
				ftp_mkdir($con, $dir_img);
			}
			$dir_page = 'public_html/images/'.$value_id;
			// $remote_file = '/demo_put'.$value.'.html';
			if($getInfo['new_path'] != ''){
				$explode_path = explode('/',$getInfo['new_path']);
				$get_file_html = array_pop($explode_path) ;
				unset($explode_path[0]);
				$temp = '';
				foreach ($explode_path as $key => $value_path) {
					ftp_mkdir($con, 'public_html'.$temp.'/'.$value_path);
					$temp .= '/'.$value_path;
				}
				$remote_file = $temp.'/'.$get_file_html;
			}
			$status = "3";
			$update = $updateLink->link($value_id,$remote_file,$status);
			//get title và content
			$title = $getInfo['title'];
			$content = $getInfo['content'];	
			//put title vào
			$partern_title = '/<title>([^<]*)<\/title>/';
			$subject_title = file_get_contents($path);
			$replace_title = "<title>".$title."</title>";
			file_put_contents($path, preg_replace($partern_title, $replace_title, $subject_title));
			//put h3 vào
			$partern_h3 = '~<h3>([^<]*)<\/h3>~';
			$subject_h3 = file_get_contents($path);
			$replace_h3 = "<h3>".$title."</h3>";
			file_put_contents($path, preg_replace($partern_h3, $replace_h3, $subject_h3));
			//put content vào
			$partern_content = '~<textarea[^>]*>[^<]*</textarea>~';
			$subject_content = file_get_contents($path);
			$replace_content = "<p>".$content."</p>";
			file_put_contents($path, preg_replace($partern_content, $replace_content, $subject_content));
			#matches ảnh
			$partern_img = '/src="([^"]+)"/';
			$subject_img = file_get_contents($path);
			//match ra src
			preg_match_all($partern_img, $subject_img, $matches, PREG_SET_ORDER, 0);

			$folder_img = ftp_nlist($con, 'public_html/images');
			$is_link =  false;
			foreach ($folder_img as $key => $link_folder_img) {
				if($link_folder_img == $value_id){
					$is_link = true;
				}
			}
			if(!$is_link){
				ftp_mkdir($con, $dir_page);
			}
			if(!empty($matches)){
			// lưu ảnh vào thư mục theo ID PAGE
				foreach ($matches as $key => $src) {
					$link_img = $src[1];
			 		$get_img = str_replace(GET_IMG, '', $link_img);
					$path_img = PATH_IMG.$get_img;
					$remote_file_img = 'public_html/images'.'/'.$value_id.'/'.$get_img;
					$result_img = ftp_put($con, $remote_file_img, $path_img, FTP_BINARY);
					$using_array_img[] = $get_img;
				}
				$thaythe = str_replace(REPLACE_PATH, URL_FTP.$value_id.'/', file_get_contents($path));
				// xóa ảnh không có trên page
				$folder_img_new = ftp_nlist($con, 'public_html/images/'.$value_id);
				$get_img_in_folder = str_replace('public_html/images/'.$value_id.'/', '', $folder_img_new);
				$array_img_in_folder[]= $get_img_in_folder;
				unset($array_img_in_folder[0]);
				unset($array_img_in_folder[1]);
				if(!empty($array_img_in_folder)){
					$img_delete = array_diff($array_img_in_folder[0], $using_array_img);
				}
				$path_delete_img = 'public_html/images/'.$value_id.'/';
				if(!empty($img_delete)){
					// unset($img_delete[0]);
					// unset($img_delete[1]);
					foreach ($img_delete as $key => $img_delete) {
						$delete = $path_delete_img.$img_delete;
						ftp_delete($con, $delete);
					}
				}
				file_put_contents($path, $thaythe);
			}
			#xử lí auto link
			$pattern_autolink = htmlentities('/\<!-- Auto link id=(.+)\-->/');
			$path_link = "https://publicgdit.000webhostapp.com";
			$abc = '';
		   	$content_autolink = htmlentities($getInfo['content']);
			$get_id_autolink =  preg_match_all($pattern_autolink, $content_autolink , $matches_autolink);
			if(!empty($matches_autolink)){
				$dir_get_link = dirname(__DIR__,2).'/template/temp.txt';
				$original_link = file_get_contents($dir_get_link);
				foreach ($matches_autolink[1] as $key => $value_id_matches_autolink) {
				$get_page_autolink = $info->selectAutoLink($value_id_matches_autolink);
					foreach ($get_page_autolink as $key => $value_get_page_autolink) {
						$explode_page_link = explode(',' , $value_get_page_autolink['page_link']);
					}
					foreach ($explode_page_link as $key => $value_page_link) {
						$get_info_page_link = $info->editPage($value_page_link);
						$link = $path_link.$get_info_page_link['new_path'];
				    	$title_get_link = $get_info_page_link['title'];
				    	$abc .= "<a href = '$link'>$title_get_link</a><br>";
				    	$partern_link = '@<a ?.*>(.*)<\/a>@';
				      	$subject_link = file_get_contents($dir_get_link);
				      	$replace_link = $abc;
				      	file_put_contents($dir_get_link, preg_replace($partern_link, $replace_link, $subject_link));
					}
					$get_txt = file_get_contents($dir_get_link);
				}
				$partern_content_autolink = '/<!-- Auto Link -->/';
				$subject_content_autolink = file_get_contents($path);
				$replace_content_autolink = $get_txt;
				file_put_contents($path, preg_replace($partern_content_autolink, $replace_content_autolink, $subject_content_autolink));
				file_put_contents($dir_get_link, $original_link);
			}
			#xử lí quảng cáo
			$pattern_ads = htmlentities('/\<!-- Area id=(.+)\-->/');
			$path_link = "https://publicgdit.000webhostapp.com";
		   	$content_ads = htmlentities($getInfo['content']);
			$get_id_ads =  preg_match_all($pattern_ads, $content_ads , $matches_ads);
			if(!empty($matches_ads)){
				$dir_get_link_ads = dirname(__DIR__,2).'/template/area.txt';
				$original_link_ads = file_get_contents($dir_get_link_ads);
				$string = '';
				$replace_area = '';
				foreach ($matches_ads[1] as $key1 => $value_matches_ads) {
					$string_area = '';
					$get_area = $info->selectAreaByID($value_matches_ads);
			      	$subject_area = file_get_contents($dir_get_link_ads);
			      	$subject_area = str_replace('area_width', $get_area[0]['width'].'%;', $subject_area);
			      	$subject_area = str_replace('area_height', $get_area[0]['height'].'%;', $subject_area);
			      	$explode_list_banner = explode(',' , $get_area[0]['list_id_banner']);
			      	$banner_length = $get_area[0]['number_sub'];
			      	$count_list_banner = count($explode_list_banner);
			      	
			      	if($count_list_banner < $banner_length ){
			      		$banner_length =  $count_list_banner;
			      	}
			      	for($i = 0 ; $i < $banner_length; $i++ ){
			      		$get_info_list_banner = $info->editBanner($explode_list_banner[$i]);
			      		if($get_area[0]['style'] == 0){
			      			$string_area .= '<img src = "'.$path_link.'/images/'.$value_id.'/'.$get_info_list_banner["link_image"].'" style = "width:'.$get_info_list_banner["width"].'%; height: '.$get_info_list_banner["height"].'px; display:block;">';
			    		
			    		}else{
			    			$string_area .= '<img src = "'.$path_link.'/images/'.$value_id.'/'.$get_info_list_banner["link_image"].'" style = "width:'.$get_info_list_banner["width"].'%; height: '.$get_info_list_banner["height"].'px; display:inline-block;">';
			    		
			    		}
			    		$path_img_banner = 'E:/xampp\htdocs\demo_intern2/image/'.$get_info_list_banner['link_image'];
						$remote_file_img_banner = 'public_html/images'.'/'.$value_id.'/'.$get_info_list_banner['link_image'];
						$result_img_banner = ftp_put($con, $remote_file_img_banner, $path_img_banner, FTP_BINARY);
			      	}
			      	$subject_area = str_replace('banner_replace', $string_area, $subject_area);
			      	$subject_content_area = file_get_contents($path);
			      	file_put_contents($path, preg_replace('<!-- Area id='.$value_matches_ads.'-->', $subject_area, $subject_content_area));
				}

			}
			// thêm feedback
			$pattern_feedback = '/<!-- feedback -->/';
			$subject_feedback = file_get_contents($path);
			$path_feedback = dirname(__DIR__,2).'/template/feedback.txt';
			$replace_feedback = file_get_contents($path_feedback);
			$replace_feedback = str_replace('value_id', $value_id, $replace_feedback);
			file_put_contents($path, preg_replace($pattern_feedback, $replace_feedback, $subject_feedback));



			// if($getInfo['template'] != ''){
			// 	$path_info_template = pathinfo($getInfo['template']);
			// 	$explode_temp = explode('/',$path_info_template['dirname']);
			// 	$get_file_temp = array_pop($explode_temp) ;
			//     $dir_temp = 'public_html/template/'.$get_file_temp;
			//     $dir_temp_css = 'public_html/template/'.$get_file_temp.'/css';
			//     $dir_temp_vendor = 'public_html/template/'.$get_file_temp.'/vendor';
			//     $dir_temp_vendor_css = 'public_html/template/'.$get_file_temp.'/vendor/css';
			//     // if(file_exists($dir_temp)){
			// 		ftp_mkdir($con, $dir_temp);
			// 	// }
			// 	// if(file_exists($dir_temp_css)){
			// 		ftp_mkdir($con, $dir_temp_css);
			// 	// }
			// 	// if(file_exists($dir_temp_js)){
			// 	// 	ftp_mkdir($con, $dir_temp_js);
			// 	// }
			// 	// if(file_exists($dir_temp_vendor)){
			// 		ftp_mkdir($con, $dir_temp_vendor);
			// 	// }
			// 	// if(file_exists($dir_temp_vendor_css)){
			// 		ftp_mkdir($con, $dir_temp_vendor_css);
			// 	// }
			// 	// if(file_exists($dir_temp_vendor_js)){
			// 	// 	ftp_mkdir($con, $dir_temp_vendor_js);
			// 	// }
			// 	$folder_css = scandir(dirname(__DIR__,2).'/template/'.$get_file_temp.'/css');
			// 	$folder_vendor_css = scandir(dirname(__DIR__,2).'/template/'.$get_file_temp.'/vendor/css');

			// 	unset($folder_css[0]);
			// 	unset($folder_css[1]);
							
			// 	unset($folder_vendor_css[0]);
			// 	unset($folder_vendor_css[1]);
				
				
			// 	$file_local_css = dirname(__DIR__,2).'/template/'.$get_file_temp.'/css/';
			// 	$file_local_vendor_css = dirname(__DIR__,2).'/template/'.$get_file_temp.'/vendor/css/';
				
			// 	foreach($folder_css as $value_folder_css){
			// 	    ftp_put($con,$dir_temp_css.'/'.$value_folder_css,$file_local_css.$value_folder_css,FTP_ASCII);
			// 	}
			
			// 	foreach($folder_vendor_css as $value_folder_vendor_css){
			// 		$path_boots = 'bootstrap.min.css';
			// 	 	if($value_folder_vendor_css == $path_boots ){
			// 	    	ftp_put($con,$dir_temp_vendor_css.'/'.$value_folder_vendor_css,$file_local_vendor_css.$value_folder_vendor_css,FTP_ASCII);
			// 		}
			// 	}
			// }
		 	#put file local
			$get_new_path = dirname(__DIR__,2).$getInfo['new_path'];
			$a = file_get_contents($path);
			file_put_contents($get_new_path, $a);
			$result_link = ftp_put($con, '/public_html'.$remote_file, $path, FTP_ASCII);
			// trả về html ban đầu
			file_put_contents($path, $original);
		}
			
		if($result_link){
			header("Location:".HOST.LIST_PATH);
		}else{
			header("Location:".HOST.UPLOAD_PATH);
		}		
	}

?>
