<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>

  <?php
    $page = new page();
    $getTC1 = $page->selectTC1();
    $controller = new PageController();
    if(isset($_POST['submit'])){
      $username = $_POST['your_email'];
      $password = $_POST['your_pass'];
      $level = $_POST['person'];
      $code = '';
      if($level == 2){
        if(isset($_POST['tc_c1']) && isset($_POST['tc_c2']) && isset($_POST['tc_c3'])){
          $code = $_POST['tc_c1'] . $_POST['tc_c2'] . $_POST['tc_c3'];
        }
      }
      if($level == 4){
        if(isset($_POST['tc_c1']) && isset($_POST['tc_c2']) && isset($_POST['tc_c3'])){
          if($_POST['tc_c1'] != '' && $_POST['tc_c2'] == '' && $_POST['tc_c3'] == '' ){
             $code = $_POST['tc_c1'] .'000000';
             $level = 6;
          }
          if($_POST['tc_c1'] != '' && $_POST['tc_c2'] != '' && $_POST['tc_c3'] == '' ){
             $code = $_POST['tc_c1'] . $_POST['tc_c2'] . '000';
             $level = 5;
          }
          if($_POST['tc_c1'] != '' && $_POST['tc_c2'] != '' && $_POST['tc_c3'] != '' ){
             $code = $_POST['tc_c1'] . $_POST['tc_c2'] . $_POST['tc_c3'];
             $level = 4;
          }
        }
      }
   
      $create = $controller->createUser($username,$password,$level,$code);
      if($create){
        header("Location:" . HOST . LIST_PATH);
      }else{
        header("Location:" . HOST . '/create-user');
      }
    }

  ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">CREATE USER</h4>
    <form class='frm_submit' method="POST"  style='width: 50%; margin: 0 auto; ' >
      <div style=" font-size: 30px; margin-left: 16px; margin-bottom: 15px; display: inline-block;  text-align:center;"  class ='button'>
         <a href="/list-user"><i class="fas fa-list"></i></a>
      </div>
     <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-10">
          <input name="your_email"  class="form-control email" id="email" placeholder="Enter email">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password:</label>
        <div class="col-sm-10">
          <input name="your_pass" type="password" class="form-control your_pass" id="pwd" placeholder="Enter password">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-5" for="cfm" >Confirm Password:</label>
        <div class="col-sm-10">
          <input name="your_pass_comfirm" type="password" class="form-control your_pass_comfirm" id = 'cfm'  placeholder="Enter confirm password">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-2">Person</label>
        <div class="col-sm-5">
        <select style="width: 50%;" class="form-control person" onchange="department()" name='person'>
          <option value="">Select</option>
          <option value="2">Creator</option>
          <option value="3">Uploader</option>
          <option value="4">Approve</option>
        </select>
      </div>
    </div>
      <div style="display: none;" class="form-group department ">
         <!--  <label style="width: 18%; display: inline-block; border: none;" class="form-control">Tổ chức cấp 1</label> -->
          <select style="display: inline-block; margin-left: 14px; width:25%;" onchange="code1()"  class="form-control col-sm-4" id ='tc_c1' name="tc_c1" >
            <option value="">---</option>
            <?php 
              foreach ($getTC1 as $key => $value) {
                $a = substr($value['code_tc'],0,3);
                $getTC2 = $page->selectTC2($a);
             ?>
              <option value="<?php echo $a ?>"><?php echo $value['name_tc']; ?></option>
            <?php
              }
            ?>
          </select>
          <select  style="display: inline-block; width:25%; margin: 0 7px 0 7px;" onchange="code2();"  class="form-control col-sm-4" name="tc_c2" id='tc_c2' >
              <option value = '' >---</option>
              
          </select>
          <select  style="display: inline-block; width:25%;"   class="form-control col-sm-4" name="tc_c3" id='tc_c3' >
              <option value = '' >---</option>
              
          </select>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button style="background-color:blue; color:white" type="submit" name="submit"  class="btn btn-default submit">Create</button>
        </div>
      </div>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){

        $('.submit').click(function(){
          var person = $('.person').val() ;
          var email = $('.email').val();
          var password = $('.your_pass').val();
          var passswordConfirm = $('.your_pass_comfirm').val();
          var check_mail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          var flag = true;
          var value_code1 = $('#tc_c1').val();
          var value_code2 = $('#tc_c2').val();
          var value_code3 = $('#tc_c3').val();
          var person = $('.person').val();
          if(email == ''){
            alert('Xin mời nhập email');
            flag = false;
          }else if(password == ''){
            alert('Xin mời nhập pass');
            flag = false;
          }else if(passswordConfirm == ''){
            alert('Xin mời nhập pass confirm');
            flag = false;
          }else if(person == ''){
            alert('Xin mời chọn person');
            flag = false;
          }else if(!check_mail.test(email)){
            alert('email không đúng định dạng');
            flag = false;
          }else if(password != passswordConfirm){
            alert('Password confirm không đúng');
            flag = false;
          }else if(email != '' && password != ''){
            $.ajax({
              async:false,
              type:'post',
              url: '/check-create-user',
              data:{email:email},
              success:function(response){
                if(response == 1){
                  alert('Email đã tồn tại');
                  flag = false;
                }
              }
            });
          }
          if(person == 2){
            if(value_code1 == '' || value_code2 == '' || value_code3 == ''){
              alert("Xin mời chọn tổ chức");
              flag = false;
            }
          }
          return flag;
        });
     });
     function department(){
      var person = $('.person').val();
      if(person == 2){
        $('.department').show();
        $('#tc_c2').attr('style','display: inline-block; width:25%; margin: 0 7px 0 7px;')
        $('#tc_c3').attr('style','display: inline-block; width:25%;')
      }else if(person == 4){
        $('.department').show();
        $('#tc_c2').attr('style','display: inline-block; width:25%; margin: 0 7px 0 7px;')
        $('#tc_c3').attr('style','display: inline-block; width:25%;')
      }else{
         $('.department').attr('style','display:none;')
      }
    }
     function code1 (){
        var value_code1 = $('#tc_c1').val();
        if(value_code1 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-tc2',
              data:{value_code1:value_code1},
              success:function(response){
                $('#tc_c2').html(response);
              } 
          });
          $('#code_1').hide();
          $('#code_2').attr('style','width: 10%; display: inline-block;');
          $('#lb_code_2').hide();
          $('#lb_code_1').attr('style','width: 10%; display: inline-block; border: none;');
          $('#lb_code_1').html(value_code1);
        }else{
          $('#code_1').show();
          $('#code_2').attr('style','width: 10%; display: none;');
          $('#code_3').attr('style','width: 10%; display: none;');
          $('#lb_code_2').show();
          $('#lb_code_2').html('000');
          $('#lb_code_3').show();
          $('#lb_code_3').html('000');
          $('#lb_code_1').hide();
          $('#tc_c2').html("<option value = '' >---</option>");
        }
        return false;
      }
      function code2 (){
        var value_code2 = $('#tc_c2').val();
        var value_code1 = $('#tc_c1').val();
        if(value_code2 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-tc3',
              data:{value_code1:value_code1,value_code2:value_code2},
              success:function(response){
                $('#tc_c3').html(response);
              } 
          });
          $('#code_2').hide();
          $('#code_3').attr('style','width: 10%; display: inline-block;');
          $('#lb_code_3').hide();
          $('#lb_code_2').attr('style','width: 10%; display: inline-block; border: none;');
          $('#lb_code_2').html(value_code2);
        }else{
          $('#code_2').show();
          $('#code_3').attr('style','width: 10%; display: none;');
          $('#lb_code_3').show();
          $('#lb_code_2').hide();
        }
        return false;
      }
    </script>