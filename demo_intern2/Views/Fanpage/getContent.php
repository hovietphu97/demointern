<?php
include_once dirname(__DIR__,2)."\Models\page.php";
include_once dirname(__DIR__,2)."\Controller\PageController.php";

$content = new PageController();
if(isset($_POST['id'])){

	$getContent = $content->edit($_POST['id']);
	
    if($getContent['template'] != ''){
        $dir  = dirname(__DIR__,2).$getContent['template'];
        $original = file_get_contents($dir);
     	$partern_title = "~<h3>([^<]*)<\/h3>~";
    	$subject_title = file_get_contents($dir);
    	$replace_title = "<h3>".$getContent['title']."</h3>";
    	file_put_contents($dir, preg_replace($partern_title, $replace_title, $subject_title));
    	
    	$partern_content = "~<textarea[^>]*>[^<]*</textarea>~";
    	$subject_content = file_get_contents($dir);
    	$replace_content = "<p>".$getContent['content']."</p>";
    	file_put_contents($dir, preg_replace($partern_content, $replace_content, $subject_content));
    	
      	$a = file_get_contents($dir);
      	file_put_contents($dir, $original);
    }	
	
	echo $a;	
}
?>