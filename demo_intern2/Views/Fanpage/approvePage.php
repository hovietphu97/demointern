<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">APPROVE</h4>
    <form method = 'POST'>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Title</th>
          <th scope="col">Status</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $page = new page();
        $get_level_user = $page->editUser($_SESSION['user_id']);
        $select_user = $page->selectUser();

        if($get_level_user['level'] == 4){
          foreach ($select_user as $key => $value_select_user) {
            if($value_select_user['level'] == 2 && $value_select_user['code'] == $get_level_user['code'] ){
              $list_page = $page->selectPageByUserID($value_select_user['id']);
            }
          }
        }

        foreach($list_page as $value){
          if($value['status'] == 4){
      ?>
      <tbody>
        <tr>
          <td><?php echo $value['title'] ?></td>
          <td><?php echo $value['status'] ?></td>
        </tr>
      </tbody>
      <?php }} ?>
    </table>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        
     });
    </script>