<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
 <?php
  $page = new page();
  $Controller = new PageController();
  $getTC1 = $page->selectTC1();
  $getTemp = $page->selectTemplate();
  $str_temp = '';
  if(isset($_POST['create_name'])){
    
    if(isset($_POST['your_name'])){
      $name = $_POST['your_name'];
    }

    if(!empty($_POST['code_1'])){
      $code = $_POST['code_1'].'000000';
    }
    if(!empty($_POST['code_2'])){
      $code = $_POST['tc_c1'].$_POST['code_2'].'000';
    }
    if(!empty($_POST['code_3'])){
      $code = $_POST['tc_c1'].$_POST['tc_c2'].$_POST['code_3'];

    if(isset($_POST['check'])){
      foreach ($_POST['check'] as $key => $value_check) {
        $str_temp .= $value_check.',';
      }
      $id_temp= substr_replace($str_temp, "" , -1);
    }
    if(isset($_POST['your_folder'])){
      $dir = dirname(__DIR__,2);
      $folder = '';
      $str_replace = str_replace('/', '', $_POST['your_folder']);
      if(!empty($str_replace)){
        mkdir($dir.$_POST['your_folder'],0777);
        $folder = $_POST['your_folder'];
      }
    }
  }
    $create = $Controller->createOrganization($name,$code,$id_temp,$folder);
    if($create){
        header("Location:" . HOST . '/list-department');
    }else{
      header("Location:" . HOST . '/organization');
    }
  }

 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">DEPARTMENT</h4>
    <form  method="POST" style=" width: 100%; display: inline-block;">
      <div style="font-size:30px; margin: 0 auto; width: 37%;" class="form-group">
          <a href="/list-department"><i class="fas fa-list"></i></a>
      </div>
      <div style="text-align: center; width: 50%; margin:0 auto; font-size: 30px;" class="form-group">
        
        <label style="width: 18%; display: inline-block; border: none;" class="form-control">Tổ chức cấp 1</label>
        <select style="display: inline-block; width:18%;" onchange="code1()"  class="form-control col-sm-4" id ='tc_c1' name="tc_c1" >
          <option value="">---</option>
          <?php 
            foreach ($getTC1 as $key => $value) {
              $a = substr($value['code_tc'],0,3);
              $getTC2 = $page->selectTC2($a);
           ?>
            <option value="<?php echo $a ?>"><?php echo $value['name_tc']; ?></option>
          <?php
            }
          ?>
        </select>
        <label style="width: 18%; display: inline-block; border: none;" class="form-control">Tổ chức cấp 2</label>
        <select  style="display: inline-block; width:21%;" onchange="code2();"  class="form-control col-sm-4" name="tc_c2" id='tc_c2' >
            <option value = '' >---</option>
            
        </select>
      </div>
        <label style="width: 10%; margin-left: 450px; display: inline-block; border: none;" class="form-group">Code: </label>

      <div style=" display: inline-block; width: 50%; margin:0 auto; font-size: 30px;" class="form-group">
        <input style="width: 10%; display: inline-block;" maxlength="3" type="text" name="code_1" class="form-control col-sm-4" id="code_1" onkeypress="myFunction1()">
        <label id="lb_code_1" style="width: 10%; display: none; border: none;" class="form-control">000</label>
        <input style="width: 10%; display: none;" maxlength="3" type="text" name="code_2" class="form-control col-sm-4" id="code_2" onkeypress="myFunction1()" >
        <label id="lb_code_2" style="width: 10%; display: inline-block; border: none;" class="form-control">000</label>
        
        <input style="width: 10%; display: none;" maxlength="3" type="text" name="code_3" class="form-control col-sm-4" id="code_3" onkeypress="myFunction1()">
        <label id="lb_code_3" style="width: 10%; display: inline-block; border: none;" class="form-control">000</label>
      </div>
      <!-- <div style=" width: 50%; margin:0 auto; display: none;" class="form-group template_lbl">
        <label style="width: 10%; display: inline-block; border: none; margin-left: 87px; margin-right: 20px;" class="form-control">Template:</label>
      </div> -->
      <label style=" vertical-align: top; width: 10%; margin-left: 424px; display: none; border: none;" class="form-group template_lbl">Template: </label>

      <div style=" width: 50%; display: none; margin:0 auto; " class="form-group template">
      <?php 
        foreach ($getTemp as $key => $value_getTemp) {
      ?>
      <input style=" display: inline-block; width: 12px;" type="checkbox" value="<?php echo $value_getTemp['id']; ?>" name="check[]" class="form-control col-sm-1" id="check">
      <label class="control-label col-md-2"><?php echo $value_getTemp['temp_name']; ?></label>
    <?php
      }
     ?>
      </div>
    <label style="width: 10%; margin-left: 450px; display: none; border: none;" class="form-group lbl_folder">Folder: </label>
    <div style="display: none;  width: 50%; margin:0 auto; font-size: 30px;" class="form-group folder">
      <input style="width: 30%; display: inline-block;" type="text" name="your_folder" class="form-control col-sm-4" id="your_folder" placeholder="Enter Folder">
      <span style="font-size: 20px; color: red; display: block"> Chỉ được nhập một folder(VD: /folder1)</span>
    </div>

    <label style="width: 10%; margin-left: 450px; display: inline-block; border: none;" class="form-group">Name: </label>
    <div style="display: inline-block;  width: 50%; margin:0 auto; font-size: 30px;" class="form-group">
      <input style="width: 30%; display: inline-block;" type="text" name="your_name" class="form-control col-sm-4" id="your_name" placeholder="Enter Name">
    </div>
      <div style="text-align: center; width: 50%; margin:0 auto; font-size: 30px;" class="form-group">

      <button style="display: inline-block; width: 18%; background: red; color:white;" class="form-control col-sm-4" name="create_name" id='create_name' type="submit">Create</button>
      </div>
  
    </form>
<script type="text/javascript">
   $(document).ready(function(){
      $("#create_name").click(function(){
          var name = $('#your_name').val();
          var value_code1 = $('#tc_c1').val();
          var value_code2 = $('#tc_c2').val();
          var code_1 = $('#code_1').val();
          var code_2 = $('#code_2').val();
          var code_3 = $('#code_3').val();
          var folder = $('#your_folder').val();
          var length_code1 = code_1.length;
          var length_code2 = code_2.length;
          var length_code3 = code_3.length;
          var flag = true;
        if(name == ''){
          alert('xin mời nhập tên');
          flag = false;
        }else if(value_code1 == ''){
            if(code_1 == ''){
              alert('xin mời nhập code 1');
              flag = false;
            }else if(length_code1 < 3){
              alert('Xin mời nhập code đủ 3 số');
              flag = false;
            }
        }else if(value_code1 != '' && value_code2 == '' ){
            if(code_2 == ''){
              alert('xin mời nhập code 2');
              flag = false;
            }else if(length_code2 < 3){
              alert('Xin mời nhập code đủ 3 số');
              flag = false;
            }
        }else if(value_code2 != ''){
            if(code_3 == ''){
              alert('xin mời nhập code 3');
              flag = false; 
            }else if(length_code3 < 3){
              alert('Xin mời nhập code đủ 3 số');
              flag = false;
            }else if(folder == ''){
              alert('Xin mời nhập folder');
              flag = false;
            }else if(folder != ''){
              console.log(1);
                $.ajax({
                async:false,
                type:'post',
                url:'/check-exist-folder',
                data:{folder:folder},
                success:function(check_folder){
                  if(check_folder == 1){
                    alert('Folder đã tồn tại');
                    flag = false;
                  }
                } 
              });
            }
        }
        if(code_1 != '' || code_2 != '' || code_3 != ''){
            $.ajax({
              async:false,
              type:'post',
              url:'/check-exist-code',
              data:{code_1:code_1,code_2:code_2,code_3:code_3,value_code1:value_code1,value_code2:value_code2},
              success:function(response){
                if(response == 1){
                  alert('Code đã tồn tại');
                  flag = false;
                }
              } 
          });
        }
        return flag;
      });
   });

  function code1 (){
    var value_code1 = $('#tc_c1').val();
    if(value_code1 != ''){
      $.ajax({
          type:'post',
          url:'/get-value-tc2',
          data:{value_code1:value_code1},
          success:function(response){
            $('#tc_c2').html(response);
          } 
      });

      $('#code_1').hide();
      $('#code_2').attr('style','width: 10%; display: inline-block;');
      $('#lb_code_2').hide();
      $('#lb_code_1').attr('style','width: 10%; display: inline-block; border: none;');
      $('#lb_code_1').html(value_code1);
      $('.template_lbl').attr('style','width: 50%; margin:0 auto; display: none;');
      $('.template').attr('style','width: 50%; margin:0 auto; display: none;');
      $('.lbl_folder').attr('style','width: 10%; margin-left: 450px; display: none; border: none;');
      $('.folder').attr('style','display: none;  width: 50%; margin:0 auto; font-size: 30px;');

    }else{
      $('#code_1').show();
      $('#code_2').attr('style','width: 10%; display: none;');
      $('#code_3').attr('style','width: 10%; display: none;');
      $('#lb_code_2').show();
      $('#lb_code_2').html('000');
      $('#lb_code_3').show();
      $('#lb_code_3').html('000');
      $('#lb_code_1').hide();
      $('#tc_c2').html("<option value = '' >---</option>");
      $('.template_lbl').attr('style','width: 50%; margin:0 auto; display: none;');
      $('.template').attr('style','width: 50%; margin:0 auto; display: none;');
      $('.lbl_folder').attr('style','width: 10%; margin-left: 450px; display: none; border: none;');
      $('.folder').attr('style','display: none;  width: 50%; margin:0 auto; font-size: 30px;');
    }
    return false;
  }

  function code2 (){
    var value_code2 = $('#tc_c2').val();
    if(value_code2 != ''){
      $('#code_2').hide();
      $('#code_3').attr('style','width: 10%; display: inline-block;');
      $('#lb_code_3').hide();
      $('#lb_code_2').attr('style','width: 10%; display: inline-block; border: none;');
      $('#lb_code_2').html(value_code2);
      $('.template_lbl').attr('style',' vertical-align: top; width: 10%; margin-left: 424px; display: inline-block; border: none;');
      $('.template').attr('style','width: 50%; display: inline-block; margin:0 auto;');
      $('.template_lbl').show();
      $('.template').show();
      $('.lbl_folder').attr('style','width: 10%; margin-left: 450px; vertical-align: top; display: inline-block; border: none;');
      $('.folder').attr('style','display: inline-block;  width: 50%; margin:0 auto; font-size: 30px;');
      $('.lbl_folder').show();
      $('.folder').show();
    }else{
      $('#code_2').show();
      $('#code_3').attr('style','width: 10%; display: none;');
      $('#lb_code_3').show();
      $('#lb_code_2').hide();
      $('.template_lbl').attr('style','width: 50%; margin:0 auto; display: none;');
      $('.template').attr('style','width: 50%; margin:0 auto; display: none;');
      $('.lbl_folder').attr('style','width: 10%; margin-left: 450px; display: none; border: none;');
      $('.folder').attr('style','display: none;  width: 50%; margin:0 auto; font-size: 30px;');
    }
    return false;
  }
  function myFunction1() {
    var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    var code_1 = $('#code_1').val();
    var code_2 = $('#code_2').val();
    var code_3 = $('#code_3').val();
    if(format.test(code_1)){
        alert('không được dùng kí tự đặc biệt');
        $('#code_1').val('');
        return false;
    }else if(format.test(code_2)){
        alert('không được dùng kí tự đặc biệt');
        $('#code_2').val('');
        return false;
    }else if(format.test(code_3)){
        alert('không được dùng kí tự đặc biệt');
        $('#code_3').val('');
        return false;
    }else if(isNaN(code_1)){
        alert('Không được nhập chữ');
        $('#code_1').val('');
        return false;
    }else if(isNaN(code_2)){
        alert('Không được nhập chữ');
        $('#code_2').val('');
        return false;
    }else if(isNaN(code_3)){
        alert('Không được nhập chữ');
        $('#code_3').val('');
        return false;
    }
  
 
}

</script>  