<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>

  <?php
    $PageController = new PageController();
    if(isset($_POST['create_area'])){
      if(isset($_POST['name_area'])){
        $name = $_POST['name_area'];
      }
      if(isset($_POST['width'])){
        $width = $_POST['width'];
      }
      if(isset($_POST['height'])){
        $height = $_POST['height'];
      }
      if(isset($_POST['number_sub'])){
        $number_sub = $_POST['number_sub'];
      }
      if(isset($_POST['style_area'])){
        $style_area = $_POST['style_area'];
      }

      $create_area = $PageController->createArea($name,$width,$height,$style_area,$number_sub);

      if($create_area){
        header("Location:" . HOST . '/ads');
      }else{
        header("Location:" . HOST . '/create-area');
      }
    }

  ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">CREATE AREA</h4>
    <form class='frm_submit' method="POST"  style='width: 50%; margin: 0 auto; ' >
      <div style=" font-size: 30px; margin-left: 16px; margin-bottom: 15px; display: inline-block;  text-align:center;"  class ='button'>
         <a href="/ads"><i class="fas fa-list"></i></a>
      </div>
     <div class="form-group">
        <label class="control-label col-sm-2" >Name:</label>
        <div class="col-sm-10">
          <input name="name_area"  class="form-control name_area" id="name_area" placeholder="Enter name area">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-5" >Width:</label>
        <label class="control-label col-sm-5" >Height:</label>
        <div style="display: inline-block;" class="col-sm-5">
          <input type="number" min="0" max="100" name="width"class="form-control width" id="width">
        </div>
        <div style="display: inline-block;" class="col-sm-5">
          <input type="number" min="0" max="100" name="height"class="form-control height" id="height">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-10" for="cfm" >Style:</label>
        <div style="display: inline-block; text-align: center;" class="col-sm-4">
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="radio" name ='style_area' class="form-control style_area" id='style_area' value="0">
          <label>Block</label>
        </div>
        <div style="display: inline-block; text-align: center;" class="col-sm-4">
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="radio" name ='style_area' class="form-control style_area" id='style_area' value="1">
          <label>Inline-block</label>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-4" >Number Sub:</label>
        <div class="col-sm-4">
          <input name="number_sub" type="number" min="0" max="100"  class="form-control number_sub" id="number_sub">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button style="background-color:blue; color:white" type="submit" name="create_area"  class="btn btn-default create_area">Create</button>
        </div>
      </div>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){

        $(".create_area").click(function(){
          var name = $('.name_area').val();
          var style_area = document.getElementsByName("style_area");
          var format = /[!@#$%^&*(),.?'":{}+=_|<>]/;
          var width = $('.width').val();
          var height = $('.height').val();
          var number_sub = $('.number_sub').val();
          var flag = true;
          if(name == ''){
              alert('chưa nhập tên area');
              flag = false;
          }else if(format.test(name)){
              alert('không được dùng kí tự đặc biệt');
              flag = false;
          }else if(width == ''){
            alert('Chưa nhập width');
            flag = false;
          }else if(height == ''){
            alert('Chưa nhập height');
            flag = false;
          }else if(number_sub == ''){
            alert('Chưa nhập số lượng phần tử con');
            flag = false;
          }else if(style_area[1].checked == false && style_area[0].checked == false){
              alert('please check Style');
              flag = false;
          }
          return flag;
        });
     });
  
    </script>