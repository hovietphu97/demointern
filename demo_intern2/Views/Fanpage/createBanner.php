<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>

  <?php
    $page = new page();
    $selectArea  = $page -> selectListArea();
    $PageController = new PageController();
    if(isset($_POST['create_banner'])){
      
      if(isset($_FILES['image'])){
        $path = dirname(__DIR__,2).'/image';
        $move = $_FILES['image']['tmp_name'];
        move_uploaded_file($move, $path.'/'.$_FILES['image']['name']);
        $image = htmlspecialchars($_POST['image_text']);
        $linkImage = $_FILES['image']['name'];
      }
      if(isset($_POST['name_banner'])){
        $name = $_POST['name_banner'];
      }
      if(isset($_POST['width'])){
        $width = $_POST['width'];
      }
      if(isset($_POST['height'])){
        $height = $_POST['height'];
      }
      if(isset($_POST['id_area'])){
        $id_area = $_POST['id_area'];
      }
      $create_banner = $PageController->createBanner($name, $image,$linkImage,$id_area,$width,$height);
      if($create_banner){
        header("Location:" . HOST . '/list-banner');
      }else{
        header("Location:" . HOST . '/create-banner');
      }
    }

  ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">CREATE BANNER</h4>
    <form class='frm_submit' method="POST" enctype="multipart/form-data"  style='width: 50%; margin: 0 auto; ' >
      <div style=" font-size: 30px; margin-left: 16px; margin-bottom: 15px; display: inline-block;  text-align:center;"  class ='button'>
         <a href="/list-banner"><i class="fas fa-list"></i></a>
      </div>
     <div class="form-group">
        <label class="control-label col-sm-2" >Name:</label>
        <div class="col-sm-10">
          <input name="name_banner"  class="form-control name_banner" id="name_banner" placeholder="Enter name banner">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" >Image:</label>
        <div class="col-sm-10">
          <input type="file" name="image"  class="form-control image" id="image" >
          <input type="hidden" name="image_text"  class="form-control image_text" id="image_text" value="">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-5" >Width:</label>
        <label class="control-label col-sm-5" >Height:</label>
        <div style="display: inline-block;" class="col-sm-5">
          <input type="number" min="0" max="100" name="width"class="form-control width" id="width">
        </div>
        <div style="display: inline-block;" class="col-sm-5">
          <input type="number" min="0" max="1000" name="height"class="form-control height" id="height">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-2">Area</label>
        <div class="col-sm-5">
          <select style="width: 50%;" class="form-control id_area"  name='id_area'>
            <option value="">Select</option>
            <?php 
              foreach ($selectArea as $key => $value) {
             ?>
            <option value="<?php echo $value['id'] ?>"><?php echo $value['name']; ?></option>
          <?php } ?>
          </select>
      </div>
    </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button style="background-color:blue; color:white" type="submit" name="create_banner"  class="btn btn-default create_banner">Create</button>
        </div>
      </div>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){

        $(".create_banner").click(function(){
          var name = $('.name_banner').val();
          var image = document.getElementsByName("image");
          var format = /[!@#$%^&*(),.?'":{}+=_|<>]/;
          var width = $('.width').val();
          var height = $('.height').val();
          var id_area = $('.id_area').val();
          var text = $('.image_text').val();
          var flag = true;
          if(name == ''){
            alert('chưa nhập tên banner');
            flag = false;
          }else if(format.test(name)){
            alert('không được dùng kí tự đặc biệt');
            flag = false;
          }else if(image == ''){
            alert('Chọn ảnh');
            flag = false;
          }
          else if(width == ''){
            alert('Chưa nhập width');
            flag = false;
          }else if(height == ''){
            alert('Chưa nhập height');
            flag = false;
          }else if(id_area == ''){
            alert('chọn area');
            flag = false;
          }
          else if(image != ''){
            // $('.image_text').attr('value',"<img src='"+"image/"+image[0].files[0].name+"' width='"+width+"%"+"' height='"+height+"%"+"'>");
            $('.image_text').attr('value',"<img src='"+"image/"+image[0].files[0].name+"' style='width:"+width+"%"+";"+"height:"+height+"px"+";'>");
            // $a = "<img style='width:"+width+";"+"height:"+height"'>"
          }
          // console.log('<img src="'+image[0].value+'" width="'+width+'%'+'" height="'+height+'%'+'">');
            // console.log(image[0].files[0].name);

          return flag;
        });
     });
  
    </script>