<?php 
	session_start();
  ob_start();
	include_once dirname(__DIR__,2)."\common\header.php" ;
  include_once dirname(__DIR__,2)."\Models\page.php";
  include_once dirname(__DIR__,2)."\config/ftp.php" ;
  include_once dirname(__DIR__,2)."\config\config.php" ;
  include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!-- <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->

 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  $page = new page();
  $getUserID = $page->selectUserID($_SESSION['email']);
  $_SESSION['user_id'] = $getUserID[0]['id'];
 ?>
 <div class="container">
    <span >
        <?php
          if(isset($_GET['success'])){
        ?>
      <div class="alert alert-success">
          <strong>Success!</strong> 
      </div>
      <?php
          }
       ?>
         <?php
          if(isset($_GET['fail'])){
        ?>
          <div class="alert alert-danger">
               <strong>Fail!</strong> 
          </div>
      <?php 
          }
         ?>
    </span>

    <?php
     if(isset($_POST['remove'])){
      if(isset($_POST['checkbox'])){
        $id_delete = $_POST['checkbox'];
        $delete = new PageController();
        $ftp = new FTP();
        $con = $ftp->ftp_connection();
        $info = new page();
        foreach ($id_delete as $key => $id) {
          // nếu có path
          $getInfo = $info->editPage($id);
          $getAutoLink = $info->selectAutoLink($id);
          if($getInfo['new_path'] != '' && $getInfo['status'] == 3){
            $explode_path = explode('/',$getInfo['new_path']);
            unset($explode_path[0]);
           #xóa file html
            foreach ($explode_path as $key => $value_explode_path) {
              if(strpos($value_explode_path,'.html')){
                 
                $get_file_html = array_pop($explode_path);
                $join_newpath  = join('/',$explode_path);
              
                $folder_new_path = ftp_nlist($con, 'public_html/'.$join_newpath);
                unset($folder_new_path[0]);
                unset($folder_new_path[1]);
                
                if(!empty($folder_new_path)){
                    $delete_file_newpath = ftp_delete($con,'public_html/'.$join_newpath.'/'.$get_file_html);
                }
              }
            }
           #xóa folder
            foreach ($explode_path as $key => $value_folder) {
              if(!strpos($value_folder,'.html')){
                $join_folder = join('/',$explode_path);
           
                ftp_rmdir($con, 'public_html/'.$join_folder);
                array_pop($explode_path);
              }
            }
          }
          $idDetete = $delete -> delete($id);
          if($getInfo['status'] == 3){
            // xóa ảnh trong thư mục
            $folder_img = ftp_nlist($con, 'public_html/images/'.$id);
            $get_img_in_folder = str_replace('public_html/images/'.$id.'/', '', $folder_img);
            unset($get_img_in_folder[0]);
            unset($get_img_in_folder[1]);
            $path_delete_img = 'public_html/images/'.$id.'/';
            foreach ($get_img_in_folder as $key => $img) {
                if(!empty($img)){
                  $delete_img = $path_delete_img.$img;
                  ftp_delete($con, $delete_img);
                }
            }
            // xóa file html 
            $folder = ftp_nlist($con, '/public_html');
            $path = "demo_put".$id.".html";
            foreach ($folder as $key => $folder) {
              if($path == $folder){
                ftp_delete($con, 'public_html/'.$path);
                
              }
            }
            // xóa thư mục 
            $path_img = 'public_html/images/';
            $folder_in_images = ftp_nlist($con, 'public_html/images');
            foreach ($folder_in_images as $key => $folder_in_images) {
              $delete_folder = $path_img.$folder_in_images;
              if($id == $folder_in_images){
                ftp_rmdir($con, $delete_folder);
              }
            }
          }
          foreach ($getAutoLink as $key => $valueAutoLink) {
            if($valueAutoLink['page_id'] == $id){
              $delete_autolink = $info->deleteAutoLink($valueAutoLink['page_id']);
              // echo "co page-id";
            }
          }
           $getLinkPage = $info->selectPageLink($id);
           $array_autoLink = array();
           // $count_autoLink = 0;
          foreach ($getLinkPage as $key => $valuePageLink) {
            if($getLinkPage != ''){
              // echo "co page link";
              $delete_pageLink = $info->deletePageLink($valuePageLink['page_link']);
              array_push($array_autoLink, $valuePageLink['page_id']);
              $_SESSION['checkbox'] = $array_autoLink;
              header("Location:upload");
            }
          }
        }
          if($idDetete){
            header("Location:".HOST.LIST_PATH);
          }else{
            header("Location:".HOST.LIST_PATH);
          }
      }
    }
    ?>
    <?php
      if(isset($_POST['unPublish'])){
        if(isset($_POST['checkbox'])){
        $id_delete = $_POST['checkbox'];
        $update = new PageController();
        $ftp = new FTP();
        $con = $ftp->ftp_connection();
        $info = new page();
        foreach ($id_delete as $key => $id) {
          // nếu có path
          $getInfo = $info->editPage($id);
          $getAutoLink = $info->selectAutoLink($id);
          if($getInfo['new_path'] != '' && $getInfo['status'] == 3){
            $explode_path = explode('/',$getInfo['new_path']);
            unset($explode_path[0]);
           #xóa file html
            foreach ($explode_path as $key => $value_explode_path) {
              if(strpos($value_explode_path,'.html')){
                 
                $get_file_html = array_pop($explode_path);
                $join_newpath  = join('/',$explode_path);
              
                $folder_new_path = ftp_nlist($con, 'public_html/'.$join_newpath);
                unset($folder_new_path[0]);
                unset($folder_new_path[1]);
                
                if(!empty($folder_new_path)){
                    $delete_file_newpath = ftp_delete($con,'public_html/'.$join_newpath.'/'.$get_file_html);
                }
              }
            }
           #xóa folder
            foreach ($explode_path as $key => $value_folder) {
              if(!strpos($value_folder,'.html')){
                $join_folder = join('/',$explode_path);
           
                ftp_rmdir($con, 'public_html/'.$join_folder);
                array_pop($explode_path);
              }
            }
          }
          if($getInfo['status'] == 3){
            // xóa ảnh trong thư mục
            $folder_img = ftp_nlist($con, 'public_html/images/'.$id);
            $get_img_in_folder = str_replace('public_html/images/'.$id.'/', '', $folder_img);
            unset($get_img_in_folder[0]);
            unset($get_img_in_folder[1]);
            $path_delete_img = 'public_html/images/'.$id.'/';
            foreach ($get_img_in_folder as $key => $img) {
                if(!empty($img)){
                  $delete_img = $path_delete_img.$img;
                  ftp_delete($con, $delete_img);
                }
            }
            // xóa file html 
            $folder = ftp_nlist($con, '/public_html');
            $path = "demo_put".$id.".html";
            foreach ($folder as $key => $folder) {
              if($path == $folder){
                ftp_delete($con, 'public_html/'.$path);
                
              }
            }
            // xóa thư mục 
            $path_img = 'public_html/images/';
            $folder_in_images = ftp_nlist($con, 'public_html/images');
            foreach ($folder_in_images as $key => $folder_in_images) {
              $delete_folder = $path_img.$folder_in_images;
              if($id == $folder_in_images){
                ftp_rmdir($con, $delete_folder);
              }
            }
          }
          $link_Unpublish = '';
          $status_Unpublish = 0;
          $update_UnPublish = $update->updateUnPublish($id,$link_Unpublish,$status_Unpublish);

        }
          // if($update_UnPublish){
          //   header("Location:".HOST.LIST_PATH);
          // }else{
          //   header("Location:".HOST.LIST_PATH);
          // }
        }
      }

    ?>

 <?php
      if(isset($_POST['upload'])){
        if(!empty($_POST['checkbox'])){
          $_SESSION['checkbox'] = $_POST['checkbox'];
          header("Location:upload");
        }
      }
  ?>
  <?php 
      if(isset($_POST['approve'])){
        if(isset($_POST['checkbox'])){
          if($getUserID[0]['level'] == 1){
            $status = '1';
          }
          if($getUserID[0]['level'] == 4){
            $status = '5';
          }
          if($getUserID[0]['level'] == 5){
            $status = '6';
          }
          if($getUserID[0]['level'] == 6){
            $status = '7';
          }
          if($getUserID[0]['level'] == 4 || $getUserID[0]['level'] == 5 || $getUserID[0]['level'] == 6){
            $update_status = $page->updateStatus($_POST['checkbox'],$status);
          }
          if($getUserID[0]['level'] == 1){
            $update_status = $page->updateStatus($_POST['checkbox'][0],$status);
          }
        }
      }
      if(isset($_POST['denine'])){
        if(isset($_POST['checkbox'])){
          if($getUserID[0]['level'] == 1){
            $status = '8';
          }
          if($getUserID[0]['level'] == 4){
            $status = '8';
          }
          if($getUserID[0]['level'] == 5){
            $status = '8';
          }
          if($getUserID[0]['level'] == 6){
            $status = '8';
          }
          if($getUserID[0]['level'] == 4 || $getUserID[0]['level'] == 5 || $getUserID[0]['level'] == 6){
            $update_status = $page->updateStatus($_POST['checkbox'],$status);
          }
          if($getUserID[0]['level'] == 1){
            $update_status = $page->updateStatus($_POST['checkbox'][0],$status);
          }
        }
      }

   ?>
<?php
      $page = new page();
      $pageController = new PageController();
      $count  = $page->count();
      $total_record =  $count[0]["count(id)"];
      if(isset($_GET['page']) && is_numeric($_GET['page'])){
          $test = ((float)$_GET['page'] - (int)$_GET['page']);
          if($test != 0) {
             
              $current_page = 1;
              header("Location:".HOST."/?page=1");
          }else{
              $current_page = (int)$_GET['page'];
              
          }
            if(is_float($_GET['page'])){ 
                header("Location:".HOST."/?page=1"); 
            } 
      } else {
       
        $current_page =1;
      }
      $limit = 5;
      // tổng số trang
      $total_page = ceil($total_record / $limit);
      $start = ($current_page - 1) * $limit;
      if($pageController->pagination($start, $limit)){

        $paging = $pageController->pagination($start, $limit);

      }else{
        $paging = '';

      } 
      
    
        $search = '';
        $searchStatus = '';
    
      if(isset($_GET['soft'])){
          
        $limit = $_GET['soft'];
        $start = ($current_page - 1) * $limit;
        $paging = $pageController->pagination($start, $limit);
        $total_page = ceil($total_record / $limit);
        if($_GET['soft'] > 20){
         header("Location:".HOST."/?page=1"); 
        }
        if(is_float($_GET['soft'])){
            header("Location:".HOST."/?page=1"); 
        }
        if(!is_numeric($_GET['soft'])){
          header("Location:".HOST."/?page=1");   
        }
        if($_GET['soft'] < 1){
            header("Location:".HOST."/?page=1"); 
        }
        
        if(isset($_GET['search_status'])){
            if(!is_numeric($_GET['search_status'])){
                header("Location:".HOST."/?page=1");   
            }else if($_GET['search_status'] < 1 || $_GET['search_status'] > 3){
                header("Location:".HOST."/?page=1"); 
            }else if(is_float($_GET['search_status'])){
                header("Location:".HOST."/?page=1"); 
            }
        }
      }
      if(isset($_GET['search']) && isset($_GET['soft'])){
        
        $limit = $_GET['soft'];
        $start = ($current_page - 1) * $limit;
        $search = trim($_GET['search']);
        $count = $pageController->count($search);
        $aaa = $pageController->search($search,$start,$limit);
        if(!empty($aaa)){
            $paging = $pageController->search($search,$start,$limit);
        }else{
            $paging = '';
        }
        $countSearch = $count[0]["count(id)"];
        $total_page = ceil($countSearch / $limit);
      }
      
    if(isset($_GET['search_status']) && isset($_GET['soft'])){
        
        $limit = $_GET['soft'];
        $start = ($current_page - 1) * $limit;
        $searchStatus = $_GET['search_status'];
        $count = $pageController->countStatus($searchStatus);
        $aaa = $pageController->searchstatus($searchStatus,$start,$limit);
        if(!empty($aaa)){
            $paging = $pageController->searchstatus($searchStatus,$start,$limit);
        }else{
            $paging = '';
        }
        $countSearchStatus = $count[0]["count(id)"];
        $total_page = ceil($countSearchStatus / $limit);
        
     }
      
      if(isset($_GET['search']) && isset($_GET['soft']) && isset($_GET['search_status'])){
         
        $limit = $_GET['soft'];
        $start = ($current_page - 1) * $limit;
        $search = trim($_GET['search']);
        $searchStatus = $_GET['search_status'];
        $count = $pageController->CountsearchAndstatus($search,$searchStatus,$start,$limit);
        $aaa = $pageController->searchAndstatus($search,$searchStatus,$start,$limit);
        if(!empty($aaa)){
            $paging = $aaa;
        }else{
            $paging = '';
        }
        $CountSearch = $count[0]["count(id)"];
        $total_page = ceil($countSearch / $limit);
        
      }
      
        $str_search = '';
        $str_searchstatus = '';
        if($search != ''){
            $str_search = '&search=';
        }
        if($searchStatus != ''){
            $str_searchstatus = '&search_status=';
        }
        if($limit != ''){
            $str_soft = '&soft=';
        }
?>

   <div style=" margin-bottom: 10px; float:left; font-size: 30px; display: inline-block;">
<?php
   if($getUserID[0]['level'] == 1){
?>
    <form class='submit_form' method="GET" style="display: inline-block;">
      <select style="display: inline-block; width:18%;" onchange="submit();" class="form-control col-sm-4" id ='soft' name="soft" >
        
        <option <?php if(isset($_GET['soft'])){ if($_GET['soft'] == 5){ echo 'selected="selected"';}} ?> value="5">5</option>
        <option <?php if(isset($_GET['soft'])){ if($_GET['soft'] == 10){ echo 'selected="selected"';}} ?> value="10">10</option>
        <option <?php if(isset($_GET['soft'])){ if($_GET['soft'] == 15){ echo 'selected="selected"';}} ?> value="15">15</option>
        <option <?php if(isset($_GET['soft'])){ if($_GET['soft'] == 20){ echo 'selected="selected"';}} ?> value="20">20</option> 
        
      </select>
      
      <select onchange="submit();" style="display: inline-block; width:21%;"  class="form-control col-sm-4" name="search_status" id='search_status' >
            <option value = '' >Select</option>
          <option <?php if(isset($_GET['search_status'])){ if($_GET['search_status'] == 1){ echo 'selected="selected"';}} ?> value="1">New</option>
          <option <?php if(isset($_GET['search_status'])){ if($_GET['search_status'] == 2){ echo 'selected="selected"';}} ?> value="2">Update</option>
          <option <?php if(isset($_GET['search_status'])){ if($_GET['search_status'] == 3){ echo 'selected="selected"';}} ?> value="3">Public</option>
        </select>
      <input style="width: 30%; display: inline-block;" type="text" name="search" class="form-control col-sm-4" id="search" placeholder="Search" onkeypress="myFunction1()" onpaste = "myFunction1()" value="<?php if(isset($_GET['search'])){echo $_GET['search'];} ?>">
      
      <button style="display: inline-block; width: 18%; background: red; color:white;" class="form-control col-sm-4" id='search_button' type="submit">Search</button>
  
  
    </form>
<?php } ?>
    <form method="GET" style="display: inline-block;">
        
    </form>
  </div>
  <form method="POST" id = 'submit_frm' action="/create-page" style="margin-top: 8px;float: right;display: inline-block;margin-left: 10px">
    <?php if( $getUserID[0]['level'] == 1 || $getUserID[0]['level'] == 2  ){ ?>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-plus" ></i></button>
    <?php } ?>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content " style="padding: 10px;">
          <div class="form-group">
            <label style="margin-right: 31px;" >Title</label>
            <input style="width: 50%;display: inline-block;" type="text" name="title" class="form-control" id="title" value=""  onkeypress="myFunction()" onpaste = "myFunction()">
            <p style="color: red; font-weight: bold;" class= "title-error"></p>
          </div>
          <div class="form-group">
            <button style="display: inline-block;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">PATH</button>
            <input style="display: inline-block; width: 50%;" type="text"  name = 'newPath' value = "" id ='newPath' class="form-control" readonly>
           <br>
          </div>
          <div class="form-group">
            <label >Template:</label>
            <div class="form-check">
              <?php
                $template = $page->selectTemplate();
                $b = array();
                if( $getUserID[0]['level'] == 2 ){
                  $template = array();
                  $get_temp_by_code = $page->editDepartment($getUserID[0]['code']);
                  if(!empty($get_temp_by_code['template_id'])){
                    $explode_temp = explode(',', $get_temp_by_code['template_id']);
                    foreach ($explode_temp as $key => $value_explode_temp) {
                      $a = $page->selectTemplateByID($value_explode_temp);
                      array_push($b, $a);
                    }
                    foreach ($b as $key => $value_b) {
                      foreach ($value_b as $key => $value_sub_b) {
                        array_push($template, $value_sub_b);
                      }
                    }
                  }
                }
               foreach ($template as $key => $temp) {
              ?>
              <label class="form-check-label" style="display: block;">
                <input type="radio" class="form-check-input" id = 'check' onclick="getTemplate('<?php echo $temp['temp_path']; ?>')" value="<?php echo $temp['temp_path']; ?>" name="optradio"> <?php echo $temp['temp_name']; ?><br>
              </label>
              <?php } ?>
            </div>
          </div>
          <div class = 'template' style="text-align:center; margin-bottom:20px;">
          </div>
           
          <button class="btn btn-primary create" type ='submit' style="width: 20%; text-align: center;">Save</button>
          <div class="modal fade" id="exampleModal" tabindex="-1"  aria-labelledby="exampleModalLabel" >
            <div class="modal-dialog" role="document">
              <div class="modal-content" style = 'width: 160%; margin-left: -150px;'>
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">SELECT PATH</h5>
                  <button type="button" class="close"  aria-label="Close">
                    <span>&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php if($getUserID[0]['level'] != 2){ ?>
                  <div class="form-group">
                    <input type="text" class="form-control" id="folder" name="folder" placeholder="Enter folder">
                    <button style="margin-top: 10px;" type="button" class="btn btn-primary" id="create_folder" name="create_folder" onclick="createfolder()">Create Folder</button>
                  </div>
                <?php } ?>
                  <div class="form-group">
                    <input type="text" class="form-control" id="html" name="html" placeholder="Enter html">
                    <button type="button" id='create' onclick="createfile()" class="btn btn-primary">Create</button>
                  </div>
                  <div class="form-group">
                    <?php
                      function dirToArray($dir) {
                         $result = array();
                         $cdir = scandir($dir);
                         foreach ($cdir as $key => $value)
                         {
                            if (!in_array($value,array(".","..","ckeditor","ckfinder","common","config","Controller","Models","Views",'index.php','template','.htaccess')))
                            {
                               if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                               {
                                  $result[$value] = $value;
                               }
                               else
                               {
                                  $result[] = $value;
                               }
                            }
                         }
                         return $result;
                       }
                      $dir = dirname(__DIR__,2);
                      $a = dirToArray($dir);
                      if( $getUserID[0]['level'] == 2){
                        $get_info_folder = $page->editDepartment($getUserID[0]['code']);
                        if(!empty($get_info_folder['folder'])){
                            function dirToArray2($dir) {
                              $page = new page();
                              $getUserID = $page->selectUserID($_SESSION['email']);
                              $get_info_folder = $page->editDepartment($getUserID[0]['code']);
                               $aa = str_replace('/', '', $get_info_folder['folder']);
                               $result = array();
                               $cdir = scandir($dir);
                               foreach ($cdir as $key => $value)
                               {
                                  if (in_array($value,array($aa)))
                                  {
                                     if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                                     {
                                        $result[$value] = $value;
                                     }
                                     else
                                     {
                                        $result[] = $value;
                                     }
                                  }
                               }
                               return $result;
                            }
                            $dir = dirname(__DIR__,2);
                            $a = dirToArray2($dir);
                          }
                      }
                      
                     

                    ?>
                    <div class="result">
                      <?php foreach( $a as $item) { ?>
                        <?php if(!strpos($item, '.html')){ ?>
                          <a href="javascript:void(0)" value="<?php echo $item; ?>" onclick = "getfolder('<?php echo $item; ?>')"  class = "<?php echo $item; ?>" name="list_folder" style="color:black"><i class="fa fa-folder" style="color:#F7D673"></i><?php  echo $item; ?></a> <br>
                          <?php }else{ ?>
                          <a href="javascript:void(0)" value="<?php echo $item; ?>"  name="getfile"><?php  echo $item; ?></a> <br>
                          <?php } ?>
                          <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <input style="display: inline-block; width: 50%;" type="text" value="" name ='path' id="path" class="form-control" disabled>
                  <button type="button" id='submit' class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>


     <form method="POST">
      
      <div style="  float:right; font-size: 30px; display: inline-block;"  class ='button'>
      
        <?php if( $getUserID[0]['level'] == 1 ){ ?>
        <button style="padding-left: 10px; border:none; background: none;" type="submit" name="unPublish" class="unPublish" id ="unPublish"><i class="fas fa-exclamation-triangle"></i></button>
        <?php } ?>
        <?php if( $getUserID[0]['level'] == 2 || $getUserID[0]['level'] == 1  ){ ?>
        <button style="padding-left: 10px; border:none; background: none;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>
        <?php } ?>
        <?php if( $getUserID[0]['level'] == 1 || $getUserID[0]['level'] == 3  ){ ?>
        <button style="padding-left: 10px; border:none; background:none; color: orange;" type="submit" class="upload" id = "upload"  name="upload"><i class="fas fa-upload"></i></button>
        <?php } ?>
       
      </div>

    <table class="table table-bordered table-hover">
      <thead class="thead-dark">
        <tr>
          <th><input type="checkbox" name="checkAll" class="check_all"></th>
          <th>Title</th>
          <th>Status</th>
          <th>Link</th>
          <th>Action</th>
          <?php
             if($getUserID[0]['level'] == 4 || $getUserID[0]['level'] == 5 || $getUserID[0]['level'] == 6 || $getUserID[0]['level'] == 1){
          ?>
          <th>Approve</th>
        <?php } ?>
          <th>Preview</th>
          <?php if(TURN == 'on'){ ?>
          <th>Version</th>
          <?php  } ?>
          <?php if(TURN_SHARE == 'on'){ ?>
          <th>Share</th>
          <?php  } ?>
          
        </tr>
      </thead>
      <?php
          $get_template_page = $page->selectTemplatePage();
          $array_follow_id = array();
          foreach ($get_template_page as $key => $valueTemplate) {
            $get_follow_id = $page->selectFollowID($valueTemplate['template']);
            foreach ($get_follow_id as $key => $value_get_follow_id) {
              array_push($array_follow_id, $value_get_follow_id);
            }
          }

          $array_follow_approve = array();
          foreach ($array_follow_id as $key => $valueArrayFollowID) {
            $get_follow_approve_id = $page->selectFollowApproveID($valueArrayFollowID['follow_id']);

            foreach ($get_follow_approve_id as $key => $value_get_follow_approve_id) {
              array_push($array_follow_approve, $value_get_follow_approve_id);
            }
          }

          foreach ($array_follow_approve as $key => $value_array_follow_approve) {
            if($getUserID[0]['level'] == 4 ){
              $get_level_user = $page->editUser($_SESSION['user_id']);
              $select_user = $page->selectUser();
              if($get_level_user['level'] == 4){
                foreach ($select_user as $key => $value_select_user) {
                  if($value_select_user['level'] == 2 && $value_select_user['code'] == $get_level_user['code'] && $value_array_follow_approve['ar1'] == 1 ){
                    $paging = $page->selectPageApproveC1($value_select_user['id']);
                  }
                }
              }
            }
          }
          foreach ($array_follow_approve as $key => $value_array_follow_approve) {
            if($getUserID[0]['level'] == 5 ){
              $controller = new PageController();
              $get_level_user = $page->editUser($_SESSION['user_id']);
              $sub_str = substr($get_level_user['code'], 0,6);
              $get_tc = $page->selectTC3($sub_str);
              $paging1 = array();
              $c = 0;
              foreach ($get_tc as $key => $value) {
                $get_user_by_code = $page -> selectUserByCode($value['code_tc']);
                if($get_user_by_code[0]['level'] == 2 ){
                  $get_page_approve_2 = $page->selectPageApproveC2($get_user_by_code[0]['id']);
                  $paging1 += array($c => $get_page_approve_2, );
                  $c++;
                }
              }
              $paging = array();
              foreach ($paging1 as $key => $v) {
                foreach ($v as $key => $y) {
                  if($value_array_follow_approve['ar2'] == 1){
                    array_push($paging,$y);
                  }
                }
              }
            }
          }
          foreach ($array_follow_approve as $key => $value_array_follow_approve) {
            if($getUserID[0]['level'] == 6 ){
              $get_level_user = $page->editUser($_SESSION['user_id']);
              $sub_str = substr($get_level_user['code'], 0,3);
              $get_tc = $page->selectTC2($sub_str);
              $paging1 = array();
              $c = 0;
              foreach ($get_tc as $key => $value_get_tc) {
                $get_user_by_code = $page -> selectUserByCode($value_get_tc['code_tc']);
                $sub_str_tc_2 = substr($get_user_by_code[0]['code'], 0,6);
                $get_tc_2 = $page->selectTC3($sub_str_tc_2);
                array_push($paging1, $get_tc_2);
              }
              $paging2 = array();
              foreach ($paging1 as $key => $value_paging1) {
                foreach ($value_paging1 as $key => $value_sub_paging1) {
                  $get_user_by_code = $page -> selectUserByCode($value_sub_paging1['code_tc']);
                  if($get_user_by_code[0]['level'] == 2 ){
                    $get_page_approve_2 = $page->selectPageApproveC3($get_user_by_code[0]['id']);
                    array_push($paging2, $get_page_approve_2);
                  }
                }
              }
              $paging = array();
              foreach ($paging2 as $key => $value_paging2) {
                foreach ($value_paging2 as $key => $value_sub_paging2) {
                  if($value_array_follow_approve['ar3'] == 1){
                    array_push($paging, $value_sub_paging2);
                  }
                }
              }
            }
          }

          $path = "https://publicgdit.000webhostapp.com";
          if($paging == ''){
      ?>
          <td colspan = 6 style = 'text-align:center;'>Không có kết quả tìm kiếm</td>
          <?php
        }else{
          foreach ($paging as $key => $value) {
      ?>
      <tbody>
        <tr>
          <?php if($value['user_id'] == $_SESSION['user_id'] &&  $getUserID[0]['level'] == 2  || $getUserID[0]['level'] == 1 || $getUserID[0]['level'] == 3 ){ ?>
            <td>
                <input type="checkbox" id = "checkbox" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
            </td>
          <?php
           }else if($getUserID[0]['level'] == 4 || $getUserID[0]['level'] == 5 || $getUserID[0]['level'] == 6){
          ?>
          <td><input type="checkbox" id = "checkbox" class="checkbox" name="checkbox" value="<?php echo $value['id'];?>"></td>
          <?php
            }else{
          ?>
          <td></td>
          <?php
            }
          ?>
            <td><?php echo $value['title']; ?></td>
           
            <td style=" padding: 0; padding-top: 12px;"><?php
                if($value['status'] == 1){
                  echo 'New';
                }else if($value['status'] == 2){
                  echo 'Update';
                }else if($value['status']==3){
                  echo 'Public';
                }else if($value['status']== 0){
                  echo "UnPublish";
                }else if($value['status'] == 4){
                  echo "Chờ Approve 1";
                }else if($value['status'] == 5){
                  echo "Chờ Approve 2";
                }else if($value['status'] == 6){
                  echo "Chờ Approve 3";
                }else if($value['status']== 7){
                  echo "Chờ Master";
                }else if($value['status']== 8){
                  echo "Denial";
                }

            ?></td>
            
            <td>  
              <a href="<?php echo $path.$value['link'] ?>" target = "_blank">
                <?php
                  if(!empty($value['link'])){
                    echo $path.$value['link'] ;
                  }
                ?>
              </a>
            </td>

            <?php if($value['user_id'] == $_SESSION['user_id'] &&  $getUserID[0]['level'] == 2  || $getUserID[0]['level'] == 1 ){ ?>
            <td>
              <a style="color:blue;padding-left: 10px;" href="/edit?id=<?php echo $value['id'];?>"><i class="fas fa-edit"></i></a>
            </td>
            
            <?php
              }else{
            ?>
            <td></td>
            <?php
              }  
            ?>

            <?php
             if($getUserID[0]['level'] == 4 || $getUserID[0]['level'] == 5 || $getUserID[0]['level'] == 6 || $getUserID[0]['level'] == 1){
                if($value['status'] == 4 || $value['status'] == 5 || $value['status'] == 6 ||$value['status'] == 7 ){
            ?>
              <td style="padding: 0; padding-top: 10px;">
                <button style=" display:inline-block; color:white; background-color: green;" type="submit" name="approve" class="btn btn-success approve" id ="approve"  ><i class="fas fa-check-circle"></i></button>
                <button style=" display: inline-block; background-color: red; color:white;" type="submit" name="denine" class="btn btn-danger denine" id ="denine"  ><i class="fas fa-ban"></i></i></button>
              </td>
            <?php }else{
              ?>
              <td></td>
              <?php
              }
            }
            ?>
            <td>
                <?php if($value['user_id'] == $_SESSION['user_id'] &&  $getUserID[0]['level'] == 2  || $getUserID[0]['level'] == 1 ){ ?>
                 <button type="button" value="<?php echo $value['id']; ?>" class="btn btn-primary preview" data-toggle="modal" data-target=".bd-example-modal-lg1">
                  <i class="fas fa-eye"></i>
                  <?php
                  }
                  ?>
            
                </button>
                <div class="modal fade bd-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body content-modal">
                       
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
            </td>
            
            <?php if($value['user_id'] == $_SESSION['user_id'] &&  $getUserID[0]['level'] == 2  || $getUserID[0]['level'] == 1 ){ ?>
            <?php if(TURN == 'on'){ ?>
            <td>
             <a href="/version?id=<?php echo $value['id']; ?>"><i class="fa fa-server" style="color: #4FC1FF"></i></a>
            </td>
           <?php } ?>
           <?php
             }else{
            ?>
              <td></td>
            <?php
              }  
            ?>
              <!-- if($value['user_id'] == $_SESSION['user_id'] &&  $getUserID[0]['level'] == 2  || $getUserID[0]['level'] == 1 ){  -->
              <!--  <td><a href="/Auto-Link?id= echo $value['id'];" ><i class="fas fa-paperclip"></i></a></td> -->
              <!--    }else{ -->
                <!-- <td></td> -->
              <!--      }   -->
              
            <?php if(TURN_SHARE == 'on'){ ?>
              <?php if($value['status'] == 3){ ?>
            <td>
              <div   class="btn btn-success clearfix "><a onclick="abc('<?php echo $path.$value['link']; ?>')"><i class="fas fa-share-square"></i></a></div>
              <input type="hidden" id="link_share<?php echo $value['id']; ?>" class="link_share" >
            </td>
            <?php
             }else{
            ?>
              <td></td>
            <?php
                  } 
            } 
            ?>
        </tr>
      </tbody>
      <?php
       }
     }
      ?>
    </table>
      <nav aria-label="Page navigation example">
        <?php if($getUserID[0]['level'] == 1 || $getUserID[0]['level'] == 2 || $getUserID[0]['level'] == 3){ ?>
        <?php if($total_page >= 2){ ?>
          <ul class="pagination">
            <?php
             if($_GET['page'] < 1){
              if(isset($_GET['search']) || isset($_GET['soft']) || isset($_GET['search_status']) ){
                header("Location:".HOST."/?page=1".$str_search.$search.$str_soft.$limit.$str_searchstatus.$searchStatus);
              }else{
                header("Location:".HOST."/?page=1");
              }
            } 
           
            ?>
            <?php
             if($_GET['page'] > $total_page){
              if(isset($_GET['search']) || isset($_GET['soft']) || isset($_GET['search_status']) ){
                header("Location:".HOST."/?page=1".$str_search.$search.$str_soft.$limit.$str_searchstatus.$searchStatus);
              }
           
              else{
                header("Location:".HOST."/?page=1");

              }
            } 
            ?>

            <?php if ($current_page > 1 && $total_page > 1){ 
            ?>
            <a class="page-link" href="?page=<?php echo ($current_page - 1);?><?php
             if(isset($_GET['search']) || isset($_GET['soft']) || isset($_GET['search_status']) ){ 
              echo $str_search.$search.$str_soft.$limit.$str_searchstatus.$searchStatus;
            }
            
            else{
              echo '';
            } 
            ?>">Previous</a>
            <?php } ?>
            <?php
              for($i = 1 ; $i<=$total_page ; $i++){
                if($i == $current_page){
            ?>    
                   <a class="page-link" ><?php echo $i; ?></a>
           <?php
                }else{
           ?>
                <a class="page-link" href="?page=<?php echo $i; ?><?php
                 if(isset($_GET['search']) || isset($_GET['soft']) || isset($_GET['search_status']) ){ 
                  echo $str_search.$search.$str_soft.$limit.$str_searchstatus.$searchStatus;
                }
               
                else{
                  echo '';
                } ?>"><?php echo $i; ?> </a>
           <?php 
                }
              }     
           ?>
           <?php
            if($current_page < $total_page && $total_page > 1){ 
           ?>
              <a class="page-link" href="?page=<?php echo ($current_page + 1); ?><?php 
              if(isset($_GET['search']) || isset($_GET['soft']) || isset($_GET['search_status']) ){ 
                echo $str_search.$search.$str_soft.$limit.$str_searchstatus.$searchStatus;
              }
           
              else{
                echo '';
              } ?>">Next</a>
            <?php } ?>
          </ul>
        <?php } ?>
        <?php } ?>
      </nav>
    </form>
    
<script type="text/javascript">
     $(document).ready(function(){
         
         $(".close").click(function(){
            $('#exampleModal').modal('hide');
        });
        
        $("#search_button").click(function(){
            var searchStatus = $('#search_status').val();
           
            var soft = $('#soft').val();
           
            
            if(searchStatus == ''){
                $('#search_status').attr('disabled','disabled');
            }
            return true;
        });
        $("#soft").click(function(){
            var searchStatus = $('#search_status').val();
            var search = $('#search').val();
            var soft = $('#soft').val();
           
            if(search == ''){
                $('#search').attr('disabled','disabled');
            }
            if(searchStatus == ''){
                $('#search_status').attr('disabled','disabled');
            }
            return true;
        });
        $("#search_status").click(function(){
           
            var search = $('#search').val();
            var soft = $('#soft').val();
           
            if(search == ''){
                $('#search').attr('disabled','disabled');
            }
            
            return true;
        });
       
        
        $(".create").click(function(e){
            var title = $('#title').val();
            var path = $('#newPath').val();
            var isChecked = $('#check:checked').val()?true:false;
            var flag = true;
            var format = /[!@#$%^&*().?":{}|<>]/;
            if(title == ''){
                alert('Vui lòng nhập title');
                flag = false;
            }else if(!isChecked){
                alert('vui lòng chọn template');
                flag = false;
            }else if(format.test(title)){
                alert('không được dùng kí tự đặc biệt');
                flag = false;
            }else if(path == ''){
                alert('Xin mời chọn path');
                flag = false;
            }
            else if(title != ''){
                
                $.ajax({
                    async: false,
                  type:'post',
                  url:'/validate-title',
                  data:{title:title},
                  success:function(respone){
                      console.log(respone);
                      if(respone == 1){
                            alert('title đã tồn tại');
                            flag = false;
                      }
                      
                    }
                });
              
            }
           return flag;
          
        });


        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        $(".unPublish").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        $(".upload").click(function(){
          var checkbox = $('.checkbox:checked').val();    
          var page_link = [];
          var boxes = $('input[name="checkbox[]"]:checked');  
          var user_id  = <?php echo $_SESSION['user_id']; ?> ;
          var flag = true;
          boxes.each(function(i,v){
            page_link.push($(v).val());
          })
          if(!checkbox){
            alert('Please check');
            flag = false;
          }else if(checkbox){
            $.ajax({
              async:false,
              type:'post',
              url:'/check-user-uploader',
              data:{page_link:page_link, user_id:user_id},
              success:function(response){
                if(response == 1){
                  alert("Có 1 số page user này không được quyền upload, xin vui lòng thử lại");
                  flag = false;
                }
              } 
            });
          }
          if(flag == true){
            if(confirm('Are you sure?')){
              flag = true;
            }else{
              flag = false;
            }
          }
          return flag;
        });
        $(".approve").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }
        });
        $(".denine").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }
        });

        $(".preview").click(function(){
          var get_id = $(this).val();      
         
          $.ajax({
              type:'post',
              url:'/get-content',
              data:{id:get_id},
              success:function(content){
                  $('.content-modal').html(content);
              }
          });

        });
        $('.check_all').click(function(){
          $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $('#search_button').click(function(){
          var search = $('#search').val();
  
          if(search == ''){
            alert('Vui lòng nhập nội dung tìm kiếm');
            return false;
          }
        });
        $('#submit').click(function(){
          var path = $('#path').val(); 
          var file = $('#html').val();
          
          if(file == ''){
            alert('bạn chưa tạo file');
            return false;
          }
          $('#exampleModal').modal('hide');
          $('#newPath').attr('value',path+'/'+file+'.html');
        });

      });

    function myFunction() {
      var title = ($("#title").val()).trim();
      
      if(title.length > 100){
        alert("Title quá 100 kí tự");
        return false;
      }
     
    }
    // function checkUploader() {
    //   var checkbox = $('.checkbox:checked').val();
    //   var page_link = [];
    //   var boxes = $('input[name="checkbox[]"]:checked');
    //   // boxes.each(function(i,v){
    //   //   page_link.push($(v).val());
    //   // })
    //   // if(!checkbox){
    //   //   alert('Please check');
    //   //   return false;
    //   // }
    //   // console.log(checkbox);
    //   return false;
    // }
    function myFunction1() {
      var search = ($("#search").val()).trim();
      var format = /[!@#$%^&*(),.?'":{}+=_|<>]/;
      
    if(format.test(search)){
        alert('không được dùng kí tự đặc biệt');
        $('#search').val('');
        return false;
    }
      if(search.length > 50){
        alert("Search quá 50 kí tự");
        return false;
      }
     
    }
 
    function getTemplate(temp) {
        var aaa = $('#check').val();
        
      
      $.ajax({
          type:'post',
          url:'/get-template',
          data:{temp:temp},
          success:function(response){
              $('.template').html(response);
          }
      });

    }
    function myFunction() {
      var title = ($("#title").val()).trim();
      if(title.length > 50){
        alert("qua 50 ki tu");
      }
    }
    function createfolder() {
      var folder = $('#folder').val();
      var path = $('#path').val();
      var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;

      if(folder == ''){
        alert('folder trống');
        return false;
      }else if(folder == 'feedback'){
        alert('không được phép tạo tên folder này');
        return false;
      }
      else if(format.test(folder)){
        alert('không được dùng kí tự đặc biệt');
        return false;
      }else{

        $.ajax({
          type:'post',
          url:'/create-folder',
          data:{folder:path+'/'+folder},
          success:function(response){
            if(response == 'exist'){
              alert('thư mục đã tồn tại');
              return false;
            }else{
              $( ".result" ).append("<a href='javascript:void(0)'  onclick = 'getfolder(\""+folder+"\")' style='color:black'><i class='fa fa-folder' style='color:#F7D673'></i>"+folder+"</a>");
            }
          }
        });
      }
    }
    function createfile() {
      var file = $('#html').val();
      var path = $('#path').val();
      var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
      if(file == ''){
        alert('file trống');
        return false;
      }else if(format.test(file)){
        alert('không được dùng kí tự đặc biệt');
        return false;
      }else{
        $.ajax({
          type:'post',
          url:'/create-file',
          data:{file:path+'/'+file},
          success:function(check_file){
            if(check_file == 'exist'){
              alert('file đã tồn tại');
              return false;
            }else{
              $( ".result" ).append("<a href='javascript:void(0)'   >"+file+".html"+"</a>");
            }
          }
        });
      }

    }
  

    function getfolder(list_folder){

      var path = $('#path').val();
      // var getFile = $('#html').attr('value');
      // console.log(getFile);
      fileValue = list_folder;
      if(path == ''){
        path = '';
      }

      $.ajax({
        type:'post',
        url:'/open-folder',
        data:{list_folder:path+'/'+list_folder},
        success:function(response){
          var BackFolder = '';
          if(fileValue != ''){
            BackFolder = "<a href='javascript:void(0)' style ='display:block;' onclick = 'backfolder()'>Back</a><br>"
          }
          $('.result').html(BackFolder+response);
        }
      });
      if(list_folder!=''){
        $('#path').attr("value",path+'/'+list_folder);
      }
    }
    function backfolder(){
      var path = $('#path').val();
      var back =  path.split('/');
      back.pop();
      var filter_array = back.filter(Boolean);
      var join_array = filter_array.join('/');
      var join_array2 = join_array.split('/');
      join_array2.pop();
      var join_array3 = '/'+join_array2.join('/')
      var getLast = back.slice(-1).pop();
     if(join_array3 == '/'){
      join_array3 = '';
     }
     $('#path').attr("value",join_array3);
     getfolder(getLast);
    }
     $(document).ready(function()
    {
      $.getScript("http://connect.facebook.net/en_US/all.js#xfbml=1", function () {
          FB.init({ appId: <?php echo APP_ID; ?>, status: true, cookie: true, xfbml: true });
      });
    });
   function abc(path){
    console.log(path);
    FB.ui(
    {
      method: 'share',
      href: path,
    },
    // callback
    function(response) {}
    );
   }
    
  </script>

</div>