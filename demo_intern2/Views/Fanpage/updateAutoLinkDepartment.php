<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
 <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
 <?php
        $Controller = new PageController();
        $page = new page();
          // var_dump($_POST);
          
        if(isset($_POST['submit'])){
          $kind_autolink = 0;
          $code_autolink = 0;
          if(isset($_POST['name_auto_link'])){
            $name_autolink= $_POST['name_auto_link'];
          }
          if(isset($_POST['status'])){
            $status_autolink = $_POST['status'];
          }
          if(!empty($_POST['category'])){
            $kind_autolink = $_POST['category'];
          }
          if(!empty($_POST['code'])){
            $code_autolink = $_POST['code'];
          }
          $index_position = $_POST['position'];
          $editID = $_POST['editID'];
          $page_link = '';
          $arr = array();
          foreach ($editID as $key => $valueID) {
            // $page_link .=$valueID.',';
            $arr += array($index_position[$key] =>  $valueID,);
          }
          ksort($arr);
          foreach ($arr as $key => $valueArr) {
            $page_link .= $valueArr.',';
          }

          $value_Page_Link_ID= substr_replace($page_link, "" , -1);


          $createAutolink = $Controller->updateAutoLinkDepartment($_GET['id'],$value_Page_Link_ID,$name_autolink,$status_autolink,$kind_autolink,$code_autolink);

          if($createAutolink){
            header("Location:".HOST.'/list-autoLink-department');
          }else{
            header("Location:".HOST.'/edit-autoLink-department?id='.$_GET['id']);
          }   
        }
     
      ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Update AutoLink Department</h4>
    <form method = 'POST'>
    <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
      <?php
        $name = '';
        $status = '';
        $kind = 0;
        $code = 0;
        if(isset($_POST['name_auto_link'])){
          $name = $_POST['name_auto_link'];
        }
        if(isset($_POST['status'])){
          $status = $_POST['status'];
        }
        if(isset($_POST['category'])){
          $kind = $_POST['category'];
        }
        if(isset($_POST['tc_c1']) && isset($_POST['tc_c2']) && isset($_POST['tc_c3'])){
          $code = $_POST['tc_c1'].$_POST['tc_c2'].$_POST['tc_c3'];
        }
      ?>
    <input type="hidden" name="name_auto_link" id="name_auto_link" value="<?php echo $name; ?>" >
    <input type="hidden" name="status" id="status" value="<?php echo $status; ?>" >
    <input type="hidden" name="category" id="category" value="<?php echo $kind; ?>" >
    <input type="hidden" name="code" id="code" value="<?php echo $code; ?>" >

   <button type="submit" class="submit" name="submit" style="color: red; border: none; background-color:#fff; padding-left: 10px;" ><i class="fas fa-plus"></i></button> 
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Page</th>
          <th scope="col">Link Page</th>
          <th scope="col">Position</th>
        </tr>
      </thead>
      <?php 
        if(isset($_POST['checkbox'])){
          $id = $_POST['checkbox'];
          foreach ($id as $key => $valueID) {
            $editPage = $page->editPage($valueID);
       ?>
      <tbody>
        <tr>
         <td><?php echo $editPage['title']; ?></td>
         <td><?php echo $editPage['new_path']; ?></td>
         <td><input type="number" name="position[]" min='1' max ='100' class="position"></td>
         <td><input type="hidden" name="editID[]" value="<?php echo $valueID; ?>"></td>

        </tr>
      </tbody>
     <?php

          }
        } 
      ?>
    </table>

    </form>
    