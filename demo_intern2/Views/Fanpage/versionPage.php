<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php";
 ?>
 <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Version Page</h4>
    <?php
      $get_ver = new page();
      $update_ver = new PageController();
      if(isset($_POST['id_version'])){
          $id_ver = $_POST['id_version'];
      }
      $get_all_info_ver = $get_ver->selectVersion($_GET['id']);
      
        
        
      if(isset($_POST['roll_back'])){

        #lấy title và nội dung cần rollback
        $get_info_ver = $get_ver -> selectVersionID($id_ver);
        $title_rollback = $get_info_ver[0]['title'];
        $content_rollback = $get_info_ver[0]['content'];

        #lấy title và thông tin hiện tại
        $get_info_now = $update_ver->edit($_GET['id']);
        $title_update = $get_info_now['title'];
        $content_update = $get_info_now['content'];

        #lấy version
        $get_ver_now = $get_ver->editVer($_GET['id']);
        $a  = preg_split ("/\ /", $get_ver_now['ver']); 
        $b = (int)$a[1] +1;
        $ver_update = $a[0].' '.$b;

        #tạo version mới cho page hiện tại
        $create_ver = $get_ver->insertVersion($title_update,$content_update,$ver_update,$_GET['id']);

        #update nội dụng cần roll back cho page
        $roll_back_page = $update_ver->updateVer($_GET['id'],$content_rollback,$title_rollback);

        if($roll_back_page){
          header("Location:".HOST.LIST_PATH);
        }
      }
    ?>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Title</th>
          <th scope="col">Version</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        if(isset($_GET['id'])){
          $id = $_GET['id'];
        }
        $page = new page();
        $listVer = $page->selectVersion($id);
        if(!empty($listVer)){

        foreach($listVer as $value){
          if($value !=  ''){
      ?>
      <tbody>
        <tr>
          <td><?php echo $value['title']; ?></td>
          <td><?php echo $value['ver']; ?></td>
          <td>
            <form method="POST" style="display: inline-block;">
              <button type="submit" value="<?php echo $value['id']; ?>" class="btn btn-primary" name="roll_back"><i class="fas fa-backward"></i></button>
              <input type="hidden" name="id_version" value="<?php echo $value['id'];?>">
            </form>
             <button type="button" value="<?php echo $value['id']; ?>" class="btn btn-primary preview" data-toggle="modal" data-target=".bd-example-modal-lg1">
                <i class="fas fa-eye"></i>
              </button>
                <div class="modal fade bd-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body content-modal">
                       
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
          </td>
        </tr>
      </tbody>
      <?php }
            }
            }else{
      ?>
            <tbody>
              <tr>
                <td colspan = 6 style = 'text-align:center; color: red; font-weight: bold;'>
                  CHƯA CÓ VERSION NÀO CHO PAGE NÀY !!!
                </td>
              </tr>
            </tbody>
      <?php
            } 
      ?>
    </table>
    </form>
<script type="text/javascript">

   $(document).ready(function(){

      $(".preview").click(function(){
          var get_id = $(this).val();      
           console.log(get_id);
          $.ajax({
              type:'post',
              url:'/get-content-version',
              data:{id:get_id},
              success:function(content){
                  $('.content-modal').html(content);
              }
          });

        });
   });

</script>