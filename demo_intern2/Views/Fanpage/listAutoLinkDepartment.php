<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">List AutoLink Department</h4>
    <form method = 'POST'>
    <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
      <a style="color: red; padding-left: 10px;" href="/Auto-Link-Department"><i class="fas fa-plus"></i></a>
      <button style="padding-left: 10px; border:none; background: none;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>
    </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Code</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $page = new page();
        $listUser = $page->selectAutoLinkDepartment();
        foreach($listUser as $value){
        $string = "<!-- Auto link id=".$value['id']."-->";
        $a = htmlentities($string);
      ?>
      <tbody>
        <tr>
          <td>
              <?php echo $value['id']; ?>
          </td>
          <td><?php echo $value['name'] ?></td>
          <td><?php echo $a;?></td>
          <td><a href="/edit-autolink-department?id=<?php echo $value['id']; ?>">Edit</a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    