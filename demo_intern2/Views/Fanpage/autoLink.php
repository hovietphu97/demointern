<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Auto Link</h4>
    <form method = 'POST' id='form_submit' action="/create-AutoLink?id=<?php echo $_GET['id']; ?>">
    <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
    <button type="button" class='button' onclick="submitForm()" style="color: red; border: none; background-color:#fff; padding-left: 10px;" ><i class="fas fa-plus"></i></button> 
    <a href="/list-AutoLink?id=<?php echo $_GET['id'] ?>"><i class="fas fa-list"></i></a>
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Page</th>
          <th scope="col">Link</th>
        </tr>
      </thead>
      <?php
        $page = new page();
        $editPage = $page->editPage($_GET['id']);
        $get_temp = $page->selectTempGetKind($editPage['template']);
        $getLink = $page->getLinkPublic($get_temp[0]['kind']);

        foreach($getLink as $value){
          if($value['id'] != $_GET['id']){
      ?>
      <tbody>
        <tr>
          <td>
          <input type="checkbox" id = "<?php echo $value['id'];?>" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
          </td>
          <td><?php echo $value['title'] ?></td>
          <td><?php  echo $value['link']; ?></td>
         
        </tr>
      </tbody>
      <?php }
      } ?>
    </table>
    </form>
    <script type="text/javascript">
      function submitForm (){
          var page_link = [];
          var flag = true;
          var checkbox = $('.checkbox:checked').val();
          var boxes = $('input[name="checkbox[]"]:checked');
          var id_page = <?php echo $_GET['id']; ?>   ;
          boxes.each(function(i,v){
            page_link.push($(v).val());
          })
          if(!checkbox){
            alert('Please check');
            flag = false;
          }
          else{
            $.ajax({
              async:false,
              type:'post',
              url:'/checkAutoLink',
              data:{page_link:page_link, id_page:id_page},
              success:function(response){
                if(response == 1){
                  alert('Title này đã tồn tại');
                  flag = false;
                }
              } 
            });
          }
          if(flag == true){
            $('#form_submit').submit();
          }

      }

</script>  
