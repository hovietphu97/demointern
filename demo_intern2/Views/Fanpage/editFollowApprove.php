<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
 <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  $page = new page();
  $edit = $page->editFollowApprove($_GET['id']);
  $Controller = new PageController();
  if(isset($_POST['upload'])){
    $ar1 = '0';
    $ar2 = '0';
    $ar3 = '0';
    if(isset($_POST['follow_approve_1'])){
      $ar1 = '1';
    }
    if(isset($_POST['follow_approve_2'])){
      $ar2 = '1';
    }
    if(isset($_POST['follow_approve_3'])){
      $ar3 = '1';
    }
    if(isset($_POST['name_approve'])){
      $name = $_POST['name_approve'];
    }
    if(isset($_POST['uploader'])){
      $uploader = $_POST['uploader'];
    }
    if(isset($_POST['master'])){
      $master = $_POST['master'];
    }
    $update = $Controller->updateFollowApproveDepartment($_GET['id'],$ar1,$ar2,$ar3,$name,$uploader,$master);
    if($update){
      header("Location:" . HOST . '/list-follow-approve');
    }else{
      header("Location:" . HOST . '/edit-follow-approve');
    }
  }

  
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">EDIT FOLLOW APPROVE</h4>
    
    <form method="POST" enctype="multipart/form-data" style='width: 50%; margin: 0 auto;' >
      <div style=" font-size: 30px; display: inline-block; text-align:center;"  class ='button'>
        <a href="/list-follow-approve"><i class="fas fa-list"></i></a>
      </div>
      <div class="form-group">
        <label style="width: 20%;">Name:</label>

        <input style="width: 50%;display: inline-block;" type="text" name ='name_approve' class="form-control name_approve" placeholder="Enter name approve" value="<?php echo $edit['name']; ?>">
      </div>
      <div class="form-group">
        <label style="width: 20%;" >Follow:</label>
        <label>Approve 1</label>
        <?php if($edit['ar1'] == 1){  ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="checkbox" name ='follow_approve_1' class="form-control follow_approve_1" value="1" checked>
        <?php }else{ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="checkbox" name ='follow_approve_1' class="form-control follow_approve_1" value="1">
        <?php } ?>
        <label>Approve 2</label>
        <?php if($edit['ar2'] == 1){  ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="checkbox" name ='follow_approve_2' class="form-control follow_approve_2" value="1" checked>
        <?php }else{ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="checkbox" name ='follow_approve_2' class="form-control follow_approve_2" value="1" >
        <?php } ?>
        <label>Approve 3</label>
        <?php if($edit['ar3'] == 1){  ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="checkbox" name ='follow_approve_3' class="form-control follow_approve_3" value="1" checked>
        <?php }else{ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="checkbox" name ='follow_approve_3' class="form-control follow_approve_3" value="1">
        <?php } ?>
      </div>
      <div class="form-group">
        <label style="width: 20%;" >Uploader:</label>
        <input style="width: 50%;display: inline-block;" type="text" name ='uploader' class="form-control uploader" value="<?php echo $edit['id_uploader']; ?>">
      </div>
      <div class="form-group">
        <label style="width: 20%;" >Master:</label>
        <label>Allow</label>
        <?php if($edit['master'] == 1){ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="radio" name ='master' class="form-control master" id='master' value="1" checked>
        <?php }else{ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="radio" name ='master' class="form-control master" id='master' value="1">
        <?php } ?> 
        <label>Don't Allow</label>
        <?php if($edit['master'] == 0){ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="radio" name ='master' class="form-control master" id='master' value="0" checked>
        <?php }else{ ?>
          <input style="width: 5%;display: inline-block; margin-right: 15px;" type="radio" name ='master' class="form-control master" id='master' value="0">
        <?php } ?> 
      </div>
      
      <button type="submit" class="btn btn-primary submit" name='upload'>Submit</button>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".submit").click(function(){
          var name = $('.name_approve').val();
          var value_master = document.getElementsByName("master");
          var value_uploader = $('.uploader').val();
          var format = /[!@#$%^&*(),.?'":{}+=_|<>]/;
          var format_uploader = /[!@#$%^&*()-.?'":{}+=_|<>]/;
          var flag = true;

          if(name == ''){
              alert('chưa nhập tên approve');
              flag = false;
          }else if(format.test(name)){
              alert('không được dùng kí tự đặc biệt');
              flag = false;
          }else if(value_master[1].checked == false && value_master[0].checked == false){
              alert('please check Master');
              flag = false;
          }else if(value_uploader != ''){
            $.ajax({
                async:false,
                url: '/check-uploader',
                type: 'POST',
                data:{value_uploader:value_uploader},
                success: function(response){
                  if(response == 1){
                    alert('ID bạn nhập không phải là uploader');
                    flag = false;
                  }
                }
            });
          }
          return flag;
        });
        
     });
    </script>