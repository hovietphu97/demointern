<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
<!--  <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div> -->
 <?php
  $page = new page();

  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
  if(isset($_POST['remove'])){
    foreach ($_POST['checkbox'] as $key => $value) {
      $delete = $page->deleteUser($value);
    }
  }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">LIST BANNER</h4>
    <form method = 'POST' style="color:white;">
    <div style="text-align: center; width: 48%; font-size: 30px;" class="form-group">
      <a style="color: red; padding-left: 10px; display: inline-block;" href="/create-banner"><i class="fas fa-plus"></i></a>
      <button style=" border:none; background: #fff;" type="submit" name="remove" class="remove" id ="remove"  ><i class="fa fa-trash"></i></button>

     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <?php
        $list_banner = $page->selectListBanner();
        foreach($list_banner as $value){
      ?>
      <tbody>
        <tr>
          <td>
          <input type="checkbox" id = "checkbox" class="checkbox" name="checkbox[]" value="<?php echo $value['id'];?>">
          </td>
          <td><?php echo $value['name'] ?></td>
          <td><a style="color:black;padding-left: 10px;" href="/edit-banner?id=<?php echo $value['id'];?>"><i class="fas fa-edit"></i></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
    </form>
    <script type="text/javascript">
     $(document).ready(function(){
    
        $(".remove").click(function(){
          var checkbox = $('.checkbox:checked').val();      
          if(!checkbox){
            alert('Please check');
            return false;
          }else if(confirm('Are you sure?')){
            return true;
          }else{
            return false;
          }
          return true;
        });
        
     });
    </script>