<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
 <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
    $page = new page();
    $Controller = new PageController();
    $getTC1 = $page->selectTC1();
    $getTemp = $page->selectTemplate();
    $edit_department = $page->editDepartmentByID($_GET['id']);
    $sub_str_tc1 = substr($edit_department['code_tc'], 0,3);
    $str_temp = '';
    $template = 0;
    if(isset($_POST['submit'])){
      if(isset($_POST['name_auto_link'])){
        $name = $_POST['name_auto_link'];
      }
      if(isset($_POST['check'])){
        foreach ($_POST['check'] as $key => $value_check) {
          $str_temp .= $value_check.',';
        }
        $template= substr_replace($str_temp, "" , -1);
      }
      $update = $Controller->updateDepart($_GET['id'], $name, $template);
      if($update){
        header("Location:" . HOST . '/list-department');
      }else{
        header("Location:" . HOST . '/edit-department?id='.$_GET['id']);
      }
    }
 ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Auto Link</h4>
    <form method = 'POST' id='form_submit' >
      
      <div style="text-align: left; width: 50%; margin:0 auto;" class="form-group">
      <div class="form-group">
        <label style="display: inline-block;" class="control-label col-sm-3" >Name AutoLink:</label>
        <input style="display: inline-block; width:30%;" name="name_auto_link"  class="form-control name_auto_link" id="name_auto_link" value="<?php echo $edit_department['name_tc']; ?>">
      </div>

      <div class="form-group">
        <select style="display: inline-block; margin-left: 14px; width:25%;" onchange="code1()"  class="form-control col-sm-4" id ='tc_c1' name="tc_c1" disabled >
            <option value="">---</option>
            <?php 
              foreach ($getTC1 as $key => $value) {
                $a = substr($value['code_tc'],0,3);
             ?>
              <option value="<?php echo $a ?>" <?php if($a == $sub_str_tc1){echo "selected";} ?>><?php echo $value['name_tc']; ?></option>
            <?php
              }
            ?>
          </select>
          <select  style="display: inline-block; width:25%; margin: 0 7px 0 7px;" onchange="code2();"  class="form-control col-sm-4" name="tc_c2" id='tc_c2' disabled >
              
          </select>
          <select  style="display: inline-block; width:25%;" disabled onchange="code3();" class="form-control col-sm-4" name="tc_c3" id='tc_c3' >
              
          </select>
      </div>
       <div style="margin:0 auto; display: block;" class="form-group template_lbl">
        <label style="width: 10%; display: inline-block; border: none;" class="form-control">Template:</label>
      </div>
      <div style="margin:0 auto; display: inline-block; width: 100%;" class="form-group template">
      <?php 
       $getValueEdit = $page->editDepartmentByID($_GET['id']);
        if(!empty($getValueEdit['template_id'])){
          $explode_temp = explode(',', $getValueEdit['template_id']);
          $count = 0; 
        foreach ($getTemp as $key => $value_getTemp) {
          if(array_key_exists($count,$explode_temp)){
            if($explode_temp[$count] == $value_getTemp['id']){
      ?>
              <input style=" display: inline-block; width: 12px;" type="checkbox" value="<?php echo $value_getTemp['id']; ?>" name="check[]" checked class="form-control col-sm-4" id="check">
              <label class="control-label col-sm-2"><?php echo $value_getTemp['temp_name']; ?></label>
          <?php 
              $count++;
            } 
          ?>
    <?php
        }else{
    ?>
        <input style=" display: inline-block; width: 12px;" type="checkbox" value="<?php echo $value_getTemp['id']; ?>" name="check[]"  class="form-control col-sm-4" id="check">
          <label class="control-label col-sm-2"><?php echo $value_getTemp['temp_name']; ?></label>
    <?php
        }
      }
    }else{
        foreach ($getTemp as $key => $value_getTemp) {

    ?>
    <input style=" display: inline-block; width: 12px;" type="checkbox" value="<?php echo $value_getTemp['id']; ?>" name="check[]"  class="form-control col-sm-4" id="check">
    <label class="control-label col-sm-2"><?php echo $value_getTemp['temp_name']; ?></label>
    <?php
      }
    }
     ?>
      </div>

      <div class="form-group">
        <button type="submit" class='btn btn-primary button' name="submit" style="color: white; background-color:blue; " >Update</button> 
      </div>
    </div>
    </form>
<script type="text/javascript">

    function code1 (){
        var value_code1 = $('#tc_c1').val();
        if(value_code1 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-tc2',
              data:{value_code1:value_code1},
              success:function(response){
                $('#tc_c2').html(response);
              } 
          });
          $('#code_1').hide();
          $('#code_2').attr('style','width: 10%; display: inline-block;');
          $('#tc_c3').html("<option value = '' >---</option>");
        }else{
          $('#code_1').show();
          $('#code_2').attr('style','width: 10%; display: none;');
          $('#code_3').attr('style','width: 10%; display: none;');
          $('#tc_c2').html("<option value = '' >---</option>");
          $('#tc_c3').html("<option value = '' >---</option>");
          $('.template_lbl').attr('style','display: none;');
          $('.template').attr('style','display: none;');
        }
        return false;
      }
      var value_code1 = $('#tc_c1').val();
      var get_id = "<?php echo $_GET['id']; ?>" ; 
        if(value_code1 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-edit-department-tc2',
              data:{value_code1:value_code1,get_id:get_id},
              success:function(response){
                $('#tc_c2').html(response);
                var value_code2 = $('#tc_c2').val();
                $.ajax({
                    type:'post',
                    url:'/get-value-edit-department-tc3',
                    data:{value_code1:value_code1,value_code2:value_code2,get_id:get_id},
                    success:function(response){
                      $('#tc_c3').html(response);
                      var value_code3 = $('#tc_c3').val();
                      if(value_code3 != ''){
                        $('.template_lbl').show();
                        $('.template').show();
                      }else{
                        $('.template_lbl').attr('style','display: none;');
                         $('.template').attr('style','display: none;');
                      }
                    }
                });
              } 
          });
        }
      function code2 (){
        var value_code2 = $('#tc_c2').val();
        var value_code1 = $('#tc_c1').val();
        if(value_code2 != ''){
          $.ajax({
              type:'post',
              url:'/get-value-tc3',
              data:{value_code1:value_code1,value_code2:value_code2},
              success:function(response){
                $('#tc_c3').html(response);
              } 
          });
          $('#code_2').hide();
          $('#code_3').attr('style','width: 10%; display: inline-block;');
        }else{
          $('#code_2').show();
          $('#code_3').attr('style','width: 10%; display: none;');
          $('#tc_c3').html('<option>---</option>');
          $('.template_lbl').attr('style',' display: none;');
          $('.template').attr('style','display: none;');
        }
        return false;
      }
      function code3 (){
        var value_code3 = $('#tc_c3').val();
        var value_code1 = $('#tc_c1').val();
        var value_code2 = $('#tc_c2').val();
        var get_id = "<?php echo $_GET['id']; ?>" ; 
        if(value_code3 != ''){
          $.ajax({
            url: '/get-template-edit-department',
            type: 'POST',
            data:{value_code1:value_code1,value_code2:value_code2,value_code3:value_code3,get_id:get_id},
            success: function(response){
                $('.template').html(response);
            }
          });
           $('.template_lbl').show();
           $('.template').show();
        }else{
          $('.template_lbl').attr('style',' display: none;');
          $('.template').attr('style','display: none;');
        }
        return false;
      }

   
      // function submitForm (){
      //     var page_link = [];
      //     var flag = true;
      //     var checkbox = $('.checkbox:checked').val();
      //     var boxes = $('input[name="checkbox[]"]:checked');
      //     boxes.each(function(i,v){
      //       page_link.push($(v).val());
      //     })
      //     if(!checkbox){
      //       alert('Please check');
      //       flag = false;
      //     }
      //     if(flag == true){
      //       $('#form_submit').submit();
      //     }

      // }

</script>  
