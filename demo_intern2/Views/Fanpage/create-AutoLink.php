<?php 
	session_start();
    ob_start();
    include_once dirname(__DIR__,2)."\common\header.php"  ;
    include_once dirname(__DIR__,2)."\Models\page.php";
    include_once dirname(__DIR__,2)."\config/ftp.php" ;
    include_once dirname(__DIR__,2)."\config\config.php" ;
    include_once dirname(__DIR__,2)."\Controller\PageController.php" ;
 ?>
 <div class='content'>
      <a href="/"><i class="fas fa-home"></i></a>
      <span class="title">Viet Phu Internship</span>
</div>
 <?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST.LOGIN_PATH);
  }
 ?>
 <?php
        $Controller = new PageController();
        $page = new page();
            
        if(isset($_POST['submit'])){
          $index_position = $_POST['position'];
          $editID = $_POST['editID'];
          $id_page = $_GET['id'];
          foreach ($editID as $key => $valueID) {
            $createAutolink = $Controller->createAutoLink($id_page,$valueID,$index_position[$key]);
          }
          if($createAutolink){
            header("Location:".HOST.'/list-AutoLink?id='.$id_page);
          }else{
            header("Location:".HOST.'/create-AutoLink?id='.$id_page);
          }   
        }
     
      ?>
    <h4 style = "text-align: center; font-size: 45px; padding: 10px 0;">Autolink</h4>
    <form method = 'POST'>
    <div style=" font-size: 30px; display: inline-block; width: 50%; text-align:center;"  class ='button'>
   <button type="submit" class="submit" name="submit" style="color: red; border: none; background-color:#fff; padding-left: 10px;" ><i class="fas fa-plus"></i></button> 
     </div>
    <table style = "margin:0 auto; width: 55%;" class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Page</th>
          <th scope="col">Link Page</th>
          <th scope="col">Position</th>
        </tr>
      </thead>
      <?php 
        if(isset($_POST['checkbox'])){
          $id = $_POST['checkbox'];
          $id_page = $_GET['id'];
          foreach ($id as $key => $valueID) {
            $editPage = $page->editPage($valueID);
       ?>
      <tbody>
        <tr>
         <td><?php echo $editPage['title']; ?></td>
         <td><?php echo $editPage['new_path']; ?></td>
         <td><input type="number" name="position[]" min='1' max ='100' class="position"></td>
         <td><input type="hidden" name="editID[]" value="<?php echo $valueID; ?>"></td>
        </tr>
      </tbody>
     <?php

          }
        } 
      ?>
    </table>

    </form>
    <script type="text/javascript">
     $(document).ready(function(){
       $('.submit').click(function(){
        var position = $('.position').val();
        var id_page = <?php echo $_GET['id']; ?>     
        var flag = true;
        if(position == ''){
          alert("Vui lòng nhập postion");
          flag = false;
        }else{
          $.ajax({
              async:false,
              type:'post',
              url:'/checkPosition',
              data:{position:position, id_page:id_page},
              success:function(response){
                if(response == 1){
                  alert('Position này đã tồn tại');
                  flag = false;
                }
              } 
          });
        }
        return flag;
       });
        
     });
    </script>