<?php session_start(); 
//dirname(__DIR__) : dùng để lui về thư mục;
?>

<?php
 	include_once dirname(__DIR__,2) . "\common\header.php"  ;

	include_once dirname(__DIR__,2)."\config\config.php" ;

  include_once dirname(__DIR__,2)."\Models\page.php" ;

  include_once dirname(__DIR__,2)."\Controller\PageController.php" ;

?>
<?php
  if(empty($_SESSION['email'])){
    header("Location:".HOST . LOGIN_PATH);
  }
 ?>

  <?php
  if(isset($_POST['submit']) && !empty($_POST['title']) && !empty($_POST['content'])){
  
    $add = new PageController();
    // ENT_QUOTES Chuyển đổi cả dấu nháy kép (") và dấu nháy đơn (').
    $title = trim(htmlspecialchars($_POST['title'])) ;
    $content = $_POST['content'];
    $newPath = '';
    
    if(!empty($_POST['newPath'])){
      $newPath = $_POST['newPath'];
    }
    $status = $_POST['status'] =  "1";

    $success = "Success";
    $fail = "Fail";
   
    $result = $add->create($title, $content, $status, $newPath);
   
    if($result){
      header("Location:".HOST.SUCCESS_PATH.$success);
    }else{
      header("Location:".HOST.FAIL_PATH.$fail);
    }
  }
?>
 <style type="text/css">
  div.login{
    width: 40%;
    margin: 0 auto;
  }
</style>
<!-- <a href="list.php" style="font-size: 100px;"><i class="fas fa-caret-left"></i></a> -->
<div class="login">
  <form name="add" action ="" method="POST">
    <div class="form-group">
      <label >Title:</label>
      <input  type="text" name="title" class="form-control" id="title" value=""  onkeypress="myFunction()" onpaste = "myFunction()">
      <p style="color: red; font-weight: bold;" class= "title-error"></p>
    </div>
    <div class="form-group">
      <label >Content:</label>
      <textarea type="text" name="content" class="form-control" id="content" rows="10" cols="150" value = "" ></textarea>
      <p style="color: red; font-weight: bold;" class= "content-error"></p>
    </div>
    <div class="form-group">
      <!-- <label >Status:</label> -->
      <input type="hidden" name="status" class="form-control" id="status"  value="New ">
    </div>

    <button style="display: inline-block;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">PATH</button>
    <input style="display: inline-block; width: 50%;" type="text" name = 'newPath' value = "" id ='newPath' class="form-control">
    <!-- <p  style="display: inline-block; width: 50%;" id='link_path' class="form-control" ></p> -->
    <button style="display: inline-block;" type="button" class="btn btn-primary" id ='clear'>CLEAR</button>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">SELECT PATH</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <input type="text" class="form-control" id="folder" name="folder" placeholder="Enter folder">
                <button style="margin-top: 10px;" type="button" class="btn btn-primary" id="create_folder" name="create_folder" onclick="createfolder()">Create Folder</button>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="html" name="html" placeholder="Enter html">
                <button type="button" id='create' onclick="createfile()" class="btn btn-primary">Create</button>

              </div>
              <div class="form-group">
                 <?php
                function dirToArray($dir) {
                 $result = array();
                 $cdir = scandir($dir);
                 foreach ($cdir as $key => $value)
                 {
                    if (!in_array($value,array(".","..","ckeditor","ckfinder","common","config","Controller","Models","Views",)))
                    {
                       if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                       {
                          $result[$value] = $value;
                       }
                       else
                       {
                          $result[] = $value;
                       }
                    }
                 }
                 return $result;
               }
                $dir = dirname(__DIR__,2);
                $a = dirToArray($dir);

              ?>
              <div class="result">
              <?php foreach( $a as $item) { ?>
                 <?php if(!strpos($item, '.html')){ ?>
            <a href="javascript:void(0)" value="<?php echo $item; ?>" onclick = "getfolder('<?php echo $item; ?>')"  class = "<?php echo $item; ?>" name="list_folder" style="color:black"><i class="fa fa-folder" style="color:#F7D673"></i><?php  echo $item; ?></a> <br>
            <?php }else{ ?>
            <a href="javascript:void(0)" value="<?php echo $item; ?>"  name="getfile"><?php  echo $item; ?></a> <br>
            <?php } ?>
            <?php } ?>
              </div>
              </div>
          </div>
          <div class="modal-footer">
            <input style="display: inline-block; width: 50%;" type="text" value="" name ='path' id="path" class="form-control" disabled>
            <button type="button" id='submit' class="btn btn-primary">Submit</button>
           
          </div>
        </div>
      </div>
    </div>
    <button style="display: block; margin-top: 10px;" type="submit" class ="submit" name='submit' class="btn btn-default">Add Page</button>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    fileValue = '';
        $(".submit").click(function(){
          var title = ($("#title").val()).trim();
          var content = CKEDITOR.instances['content'].getData();
          if(title.length > 50){
            $('.title-error').text("title quá 50 kí tự");
            return false;
          }
          if(title == "" && content == ""){
            $('.title-error').text("title error");
            $('.content-error').text("content error");
            return false;
          }else if(title == ""){
            $('.content-error').text("");
            $('.title-error').text("title error");
            return false;
          }else if(content == ""){
            $('.title-error').text("");
            $('.content-error').text("content error");
            return false;
          }else{
            $('.content-error').text("");
          }
          return true;

        });

       

        
        $('#submit').click(function(){
          var path = $('#path').val(); 
          var file = $('#html').val();
          
          if(file == ''){
            alert('bạn chưa tạo file');
            return false;
          }
          $('#newPath').attr('value',path+'/'+file+'.html');
        });

       
       

      
  });
    function myFunction() {
      var title = ($("#title").val()).trim();
      if(title.length > 50){
        alert("qua 50 ki tu");
      }
    }
    function createfolder() {
      var folder = $('#folder').val();
      var path = $('#path').val();
      var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;

      if(folder == ''){
        alert('folder trống');
        return false;
      }else if(format.test(folder)){
        alert('không được dùng kí tự đặc biệt');
        return false;
      }else{

        $.ajax({
          type:'post',
          url:'createFolder.php',
          data:{folder:path+'/'+folder},
          success:function(response){
            if(response == 'exist'){
              alert('thư mục đã tồn tại');
              return false;
            }else{
              $( ".result" ).append("<a href='javascript:void(0)'  onclick = 'getfolder(\""+folder+"\")' style='color:black'><i class='fa fa-folder' style='color:#F7D673'></i>"+folder+"</a>");
            }
          }
        });
      }
    }
    function createfile() {
      var file = $('#html').val();
      var path = $('#path').val();
      var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
      if(file == ''){
        alert('file trống');
        return false;
      }else if(format.test(file)){
        alert('không được dùng kí tự đặc biệt');
        return false;
      }else{
        $.ajax({
          type:'post',
          url:'createFile.php',
          data:{file:path+'/'+file},
          success:function(response){
            if(response == 'exist'){
              alert('file đã tồn tại');
              return false;
            }else{
              $( ".result" ).append("<a href='javascript:void(0)'   >"+file+".html"+"</a>");
            }
          }
        });
      }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    }
    // function getFile(getFile){
    //   var path = $('#path').val();
    //   if(getFile!=''){
    //       $('#path').val(getFile);
    //   }
    // }


    function getfolder(list_folder){

      var path = $('#path').val();
      // var getFile = $('#html').attr('value');
      // console.log(getFile);
      fileValue = list_folder;
      if(path == ''){
        path = '';
      }

      $.ajax({
        type:'post',
        url:'openfolder.php',
        data:{list_folder:path+'/'+list_folder},
        success:function(response){
          var BackFolder = '';
          if(fileValue != ''){
            BackFolder = "<a href='javascript:void(0)' style ='display:block;' onclick = 'backfolder()'>Back</a><br>"
          }
          $('.result').html(BackFolder+response);
        }
      });
      if(list_folder!=''){
        $('#path').attr("value",path+'/'+list_folder);
      }
    }
    function backfolder(){
      var path = $('#path').val();
      var back =  path.split('/');
      back.pop();
      var filter_array = back.filter(Boolean);
      var join_array = filter_array.join('/');
      var join_array2 = join_array.split('/');
      join_array2.pop();
      var join_array3 = '/'+join_array2.join('/')
      var getLast = back.slice(-1).pop();
     if(join_array3 == '/'){
      join_array3 = '';
     }
     $('#path').attr("value",join_array3);
     getfolder(getLast);
    }
    // function submit(){
    //   var path = $('#path').val(); 
    //   var file = $('#html').val();
    //   $('#newPath').attr('value',path+'/'+file);
    // }

</script>




<script>
    // Thay thế <textarea id="post_content"> với CKEditor
    CKEDITOR.replace( 'content' , {
      filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
      filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
     });// tham số là biến name của textarea

</script>