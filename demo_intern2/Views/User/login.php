<?php session_start(); 
//dirname(__DIR__) : dùng để lui về thư mục;
?>

<?php
 	include_once dirname(__DIR__,2)."/common/header.php";
	include_once dirname(__DIR__,2)."/config/config.php" ;
  include_once dirname(__DIR__,2)."/Models/page.php" ;
?>


<style type="text/css">
	div.login{
		width: 40%;
		margin: 0 auto;
    margin-top: 50px;
    padding: 17px;
    border: 2px solid green;
	}
    div.content{
      width: 100%;
      background: black;
      color:orange;
      text-align: center; 
      font-size: 40px;
      font-weight: bold;  
    }
</style>
<?php
  if(isset($_SESSION['email'])){
    header("Location:".HOST.LIST_PATH);
  }
  $login = new page();
  $user = $login->selectUser();
  if(isset($_POST['sign'])){
    
    foreach ($user as $key => $value) {
      if($value['username'] == $_POST['your_email'] && $value['password'] == $_POST['your_pass'] ){
       $_SESSION['email'] = $_POST['your_email'];
       header("Location:" . HOST . LIST_PATH);
      }
    }
    // exit;
  }

?>
<div class="login">
  <h2 style="color:blue; text-align: center;">Account Login</h2>
  <form style="margin-left: 63px;" method="POST" class="form-horizontal login">

      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-10">
          <input name="your_email"  class="form-control" id="email" placeholder="Enter email">
          <p style="color: red; font-weight: bold;" class= "email-error"></p>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password:</label>
        <div class="col-sm-10">
          <input name="your_pass" type="password" class="form-control" id="pwd" placeholder="Enter password">
          <p style="color: red; font-weight: bold;" class= "pass-error"></p>
        </div>
      </div>
        
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button style="color:white; background-color: blue;" type="submit" name="sign"  class="btn btn-default submit">Login</button>
        </div>
        
      </div>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
      

      $(".submit").click(function(){ 
        var email = $("#email").val().trim();
        var pass = $("#pwd").val().trim();
        var check_mail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var flag = true;
          if(email == ''){
            alert('Vui lòng nhập email');
            flag = false;
          }else if(!check_mail.test(email)){
            alert("Không đúng định dạng Email");
            flag = false;
          }else if(pass == ''){
            alert("Vui lòng nhập pass")
            flag = false;
          }else if( email != "" && pass != "" ){
            $.ajax({
                async:false,
                type:'post',
                url: '/check-user',
                data:{email:email,pass:pass},
                success:function(response){
                    if(response == 1){
                        alert("Email or Pass không đúng ! ");
                        flag = false;
                    }
                }
            });
          }
     return flag;
    });
  });

</script>

</body>
</html>